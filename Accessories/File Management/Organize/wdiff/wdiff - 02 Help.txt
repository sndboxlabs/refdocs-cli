wdiff - Compares words in two files and report differences.

Usage: wdiff [OPTION]... FILE1 FILE2
   or: wdiff -d [OPTION]... [FILE]

Mandatory arguments to long options are mandatory for short options too.
  -C, --copyright            display copyright then exit
  -1, --no-deleted           inhibit output of deleted words
  -2, --no-inserted          inhibit output of inserted words
  -3, --no-common            inhibit output of common words
  -a, --auto-pager           automatically calls a pager
  -d, --diff-input           use single unified diff as input
  -h, --help                 display this help then exit
  -i, --ignore-case          fold character case while comparing
  -l, --less-mode            variation of printer mode for "less"
  -n, --avoid-wraps          do not extend fields through newlines
  -p, --printer              overstrike as for printers
  -s, --statistics           say how many words deleted, inserted etc.
  -t, --terminal             use termcap as for terminal displays
  -v, --version              display program version then exit
  -w, --start-delete=STRING  string to mark beginning of delete region
  -x, --end-delete=STRING    string to mark end of delete region
  -y, --start-insert=STRING  string to mark beginning of insert region
  -z, --end-insert=STRING    string to mark end of insert region

Report bugs to <wdiff-bugs@gnu.org>.
