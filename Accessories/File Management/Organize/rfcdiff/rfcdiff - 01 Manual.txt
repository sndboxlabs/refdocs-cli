RFCDIFF(1)                                User Commands                                RFCDIFF(1)

NAME
       rfcdiff - compare RFCs or Internet Drafts or any other text files

DESCRIPTION
              rfcdiff [options] file1 file2

              rfcdiff  takes two RFCs or Internet-Drafts in text form as input, and produces out‐
              put which indicates the differences found in one of various  forms,  controlled  by
              the  options listed below. In all cases, page headers and page footers are stripped
              before looking for changes.

       --html Produce side-by-side .html diff (default)

       --chbars
              Produce changebar marked .txt output

       --diff Produce a regular diff output

       --wdiff
              Produce paged wdiff output

       --hwdiff
              Produce html-wrapped coloured wdiff output

       --oldcolour COLOURNAME
              Colour for new file in hwdiff (default is "green")

       --oldcolor COLORNAME
              Color for old file in hwdiff (default is "red")

       --newcolour COLOURNAME
              Colour for new file in hwdiff (default is "green")

       --newcolor COLORNAME
              Color for new file in hwdiff (default is "green")

       --larger
              Make difference text in hwdiff slightly larger

       --browse
              Show html output in browser

       --keep Don't delete temporary workfiles

       --version
              Show version

       --help Show this help

       --info "Synopsis|Usage|Copyright|Description|Log"
              Show various info

       --width N
              Set a maximum width of N characters for the display of each  half  of  the  old/new
              html diff

       --linenum
              Show linenumbers for each line, not only at the start of each change section

       --body Strip  document  preamble  (title, boilerplate and table of contents) and postamble
              (Intellectual Property Statement, Disclaimer etc)

       --nostrip
              Don't strip headers and footers (or body)

       --ab-diff
              Before/After diff, suitable for rfc-editor

       --abdiff

       --stdout
              Send output to stdout instead to a file

AUTHOR
       This manual page was written by Peter Spiess-Knafl  <dev@spiessknafl.at>  for  the  Debian
       GNU/Linux system (but may be used by others).

rfcdiff 1.45                                April 2016                                 RFCDIFF(1)
