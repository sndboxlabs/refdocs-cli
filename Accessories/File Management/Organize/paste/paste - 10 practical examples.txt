#################################
# paste - 10 practical examples #
#################################

From: https://mittinsingh.wordpress.com/2016/05/16/paste-command-with-10-practical-examples/

by Mittinpreet Singh Nayyar

linux paste command is used to merge lines of files. In this paste command we
have two options along with paste command -d and -s

    -d = delimiters

    -s = serial

Syntax of paste command
-----------------------

    paste [options] files

Now are going to see how to make use of -d and -s options to combine different
file output.

In this article we are going to use below two files for demonstrate purpose.

    [root@mittin ~]# cat mittinfile1
    Tech
    Tututorial
    Arkit
    WebServer
    paste
    command
    [root@mittin ~]# cat mittinfile2
    Tutorial
    Tech
    Linux
    server
    command

Joining multiple files using paste command
------------------------------------------

To join the file content side by side just we have to below command

    [root@mittin ~]# paste mittinfile1 mittinfile2
    Tech    Tutorial
    Tututorial      Tech
    Arkit   Linux
    WebServer       server
    paste   command
    command

Joining multiple files separated by specified delimiter
-------------------------------------------------------

In this example we are going to use comma(,) as delimiter

    [root@mittin ~]# paste -d, mittinfile1 mittinfile2
    Tech,Tutorial
    Tututorial,Tech
    Arkit,Linux
    WebServer,server
    paste,command
    command,

Merging multiple files sequentially
-----------------------------------

To merge multiple files in sequentially we have to use -s option along with
paste command as shown in below example

    [root@mittin ~]# paste -s mittinfile1 mittinfile2
    Tech    Tututorial      Arkit   WebServer       paste   command
    Tutorial        Tech    Linux   server  command

Separate file data as 2 columns
-------------------------------

Straight line of data will be shown as two separate columns example shown below

    [root@mittin ~]# cat mittinfile1   <===== Before
    Tech
    Tututorial
    Arkit
    WebServer
    paste
    command
    [root@mittin ~]# paste - - < mittinfile1   <== After
    Tech    Tututorial
    Arkit   WebServer
    paste   command

Join multiple lines as single line separated by comma delimiter
---------------------------------------------------------------

From below content

    [root@mittin ~]# cat mittinfile1
    Tech
    Tututorial
    Arkit
    WebServer
    paste
    command

Changed to below

    [root@mittin ~]# paste -d, -s mittinfile1
    Tech,Tututorial,Arkit,WebServer,paste,command

Merging both file content alternatively
---------------------------------------

We have two files which are having number of lines merging them alternatively
we have to use below command

    [root@mittin ~]# paste -d'\n' -s mittinfile1 mittinfile2
    Tech
    Tututorial
    Arkit
    WebServer
    paste
    command
    Tutorial
    Tech
    Linux
    server
    command

Merging file content separated by semicolon
-------------------------------------------

    [root@mittin ~]# paste -d':' - - < mittinfile1
    Tech:Tututorial
    Arkit:WebServer
    paste:command

Merging a file into 3 columns using multiple delimiters
-------------------------------------------------------

    [root@mittin ~]# paste -d:, - - - < mittinfile1
    Tech:Tututorial,Arkit
    WebServer:paste,command

 

Conclusion

We can make use of paste command to merge lines of multiple files and modify as
required using paste options -d and -s.

Thanks for the read.

This entry was posted in Linux on May 16, 2016 by mittinsingh.
