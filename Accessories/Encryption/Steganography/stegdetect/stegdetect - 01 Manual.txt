STEGDETECT(1)                      BSD General Commands Manual                      STEGDETECT(1)

NAME
     stegdetect — finds image files with steganographic content

SYNOPSIS
     stegdetect [-qhnV] [-s float] [-C num,tfname] [-c file ... name] [-D file] [-d num]
                [-t tests] [file ...]

DESCRIPTION
     The stegdetect utility analyses image files for steganographic content.  It runs statistical
     tests to determine if steganographic content is present, and also tries to find the system
     that has been used to embed the hidden information.

     The options are as follows:

     -q        Only reports images that are likely to have steganographic content.

     -h        Only calculates the DCT histogram.  Use the -d option to display the values.

     -n        Enables checking of JPEG header information to surpress false positives.  If
               enabled, all JPEG images that contain comment fields will be treated as negatives.
               OutGuess checking will be disabled if the JFIF marker does not match version 1.1.

     -V        Displays the version number of the software.

     -s float  Changes the sensitivity of the detection algorithms.  Their results are multiplied
               by the specified number.  The higher the number the more sensitive the test will
               become.  The default is 1.

     -C num,tfname
               Feature vectors are being extraced from the images.  The argument num can either
               be zero or one.  A zero indicates that the provided images do not contain stegano‐
               graphic content, a one indicates that they do.  The argument tfname is the name of
               transform used for feature extraction.  The features vectores are printed to
               stdout.

     -c file   Reads the data created by the -C options and computes the necessary values to
               detect steganographic content in yet unknown images.  The option can be used mul‐
               tiple times.  It expects that the name of the scheme provided as additional argu‐
               ment.  The result is a decision object that can be used with the -D option.  The
               decision object contains a the parameters for a linear discriminant function based
               on the Neyman-Pearson theorem.

     -D file   Reads a decision object that contains detection information about a new stegano‐
               graphic scheme.

     -d num    Prints debug information.

     -t tests  Sets the tests that are being run on the image.  The following characters are
               understood:

               j       Tests if information has been embedded with jsteg.

               o       Tests if information has been embedded with outguess.

               p       Tests if information has been embedded with jphide.

               i       Tests if information has been hidden with invisible secrets.

               f       Tests if information has been hidden with F5.

               F       Tests if information has been hidden with F5 using a more sophisticated
                       but fairly slow detection algorithm.

               a       Tests if information has been added at the end of file, for example by
                       camouflage or appendX.

               The default value is jopifa.

     The stegdetect utility indicates the accuracy of the detection with a number of stars behind
     the detected system.  If no filenames have been specified, stegdetect will read the file‐
     names from stdin.

EXAMPLES
     stegdetect -t p auto.jpg

     Tries to detect the presence of jphide embedded information in auto.jpg.

ERRORS
     stegdetect works only for JPEG images.

     Currently, there is no support for parameter training.  The only exported knob is the sensi‐
     tivity level.  Future versions will export all detection parameters via a configuration
     file.

SEE ALSO
     stegbreak(1)

ACKNOWLEDGEMENTS
     This product includes software developed by Ian F. Darwin and others.  The stegdetect util‐
     ity uses Darwin's file magic to identify data appended at the end of an image.

AUTHORS
     The stegdetect utility has been developed by Niels Provos.

BSD                                       April 01, 2001                                      BSD
