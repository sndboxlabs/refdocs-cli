Usage: factor [NUMBER]...
  or:  factor OPTION
Print the prime factors of each specified integer NUMBER.  If none
are specified on the command line, read them from standard input.

      --help     display this help and exit
      --version  output version information and exit

GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
Full documentation at: <http://www.gnu.org/software/coreutils/factor>
or available locally via: info '(coreutils) factor invocation'
