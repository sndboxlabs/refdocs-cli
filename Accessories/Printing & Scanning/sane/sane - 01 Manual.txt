sane(7)                            SANE Scanner Access Now Easy                           sane(7)

NAME
       sane - Scanner Access Now Easy: API for accessing scanners

DESCRIPTION
       SANE  is  an  application programming interface (API) that provides standardized access to
       any raster image scanner hardware. The standardized interface makes it possible  to  write
       just  one driver for each scanner device instead of one driver for each scanner and appli‐
       cation.

       While SANE is primarily targeted at a UNIX environment, the standard  has  been  carefully
       designed  to  make it possible to implement the API on virtually any hardware or operating
       system.

       This manual page provides a summary of the information available about SANE.

       If you have trouble getting your scanner detected, read the PROBLEMS section.

TERMINOLOGY
       An application that uses the SANE interface is called a  SANE  frontend.   A  driver  that
       implements  the  SANE  interface  is  called a SANE backend.  A meta backend provides some
       means to manage one or more other backends.

SOFTWARE PACKAGES
       The package `sane-backends' contains a lot of backends, documentation (including the  SANE
       standard),  networking  support, and the command line frontend `scanimage'.  The frontends
       `xscanimage', `xcam', and `scanadf' are included in the  package  `sane-frontends'.   Both
       packages  can be downloaded from the SANE homepage (http://www.sane-project.org/).  Infor‐
       mation about other frontends and backends can also be found on the SANE homepage.

GENERAL INFORMATION
       The following sections provide short descriptions and links to more information about sev‐
       eral aspects of SANE.  A name with a number in parenthesis (e.g.  `sane-dll(5)') points to
       a manual page. In this  case  `man  5  sane-dll'  will  display  the  page.  Entries  like
       `/usr/share/doc/libsane/sane.tex'  are  references  to  text files that were copied to the
       SANE documentation directory  (/usr/share/doc/libsane/)  during  installation.  Everything
       else is a URL to a resource on the web.

       SANE homepage
         Information  on  all aspects of SANE including a tutorial and a link to the SANE FAQ can
         be found on the SANE homepage: http://www.sane-project.org/.

       SANE device lists
         The SANE device lists contain information about the status of SANE support  for  a  spe‐
         cific  device.  If  your  scanner is not listed there (either supported or unsupported),
         please contact us. See section HOW CAN YOU HELP SANE for details. There  are  lists  for
         specific  releases  of  SANE,  for  the current development version and a search engine:
         http://www.sane-project.org/sane-supported-devices.html.  The lists are  also  installed
         on your system at /usr/share/doc/libsane/.

       SANE mailing list
         There  is  a mailing list for the purpose of discussing the SANE standard and its imple‐
         mentations: sane-devel.  Despite its name, the list is not only intended for developers,
         but  also  for  users.  There  are also some more lists for special topics, however, for
         users,  sane-devel  is   the   right   list.   How   to   subscribe   and   unsubscribe:
         http://www.sane-project.org/mailing-lists.html.

       SANE IRC channel
         The  IRC  (Internet  Relay  Chat)  channel  #sane  can  be found on the Freenode network
         (irc.freenode.net). It's for discussing SANE problems,  talking  about  development  and
         general  SANE related chatting. Before asking for help, please read the other documenta‐
         tion mentioned in this manual page. The channel's topic is also used  for  announcements
         of problems with SANE infrastructure (mailing lists, web server, etc.).

       Compiling and installing SANE
         Look  at /usr/share/doc/libsane/README and the os-dependent README files for information
         about compiling and installing SANE.

       SCSI configuration
         For information about various systems and SCSI controllers see sane-scsi(5).

       USB configuration
         For information about USB configuration see sane-usb(5).

FRONTENDS AND MISCELLANEOUS PROGRAMS
       scanimage
         Command-line frontend. See scanimage(1).

       saned
         SANE network daemon that allows remote  clients  to  access  image  acquisition  devices
         available on the local host. See saned(8).

       sane-find-scanner
         Command-line  tool  to find SCSI and USB scanners and determine their Unix device files.
         See sane-find-scanner(1).

       Also, have a look at the sane-frontends package (including xscanimage, xcam, and  scanadf)
       and the frontend information page at http://www.sane-project.org/sane-frontends.html.

BACKENDS FOR SCANNERS
       abaton
         The  SANE backend for Abaton flatbed scanners supports the Scan 300/GS (8bit, 256 levels
         of gray) and the Scan 300/S (black and white, untested). See sane-abaton(5) for details.

       agfafocus
         This backend supports AGFA  Focus  scanners  and  the  Siemens  S9036  (untested).   See
         sane-agfafocus(5) for details.

       apple
         The  SANE backend for Apple flatbed scanners supports the following scanners: AppleScan‐
         ner, OneScanner and ColorOneScanner. See sane-apple(5) for details.

       artec
         The SANE Artec backend supports several Artec/Ultima SCSI flatbed scanners  as  well  as
         the BlackWidow BW4800SP and the Plustek 19200S. See sane-artec(5) for details.

       artec_eplus48u
         The  SANE  artec_eplus48u backend supports the scanner Artec E+ 48U and re-badged models
         like Tevion MD 9693, Medion MD 9693, Medion MD 9705 and Trust Easy  Webscan  19200.  See
         sane-artec_eplus48u(5) for details.

       as6e
         This  is  a  SANE  backend for using the Artec AS6E parallel port interface scanner. See
         sane-as6e(5) for details.

       avision
         This backend supports several Avision based scanners. This includes the original Avision
         scanners  (like  AV  630,  AV  620, ...) as well as the HP ScanJet 53xx and 74xx series,
         Fujitsu ScanPartner, some Mitsubishi and Minolta film-scanners.  See sane-avision(5) for
         details.

       bh
         The  bh backend provides access to Bell+Howell Copiscan II series document scanners. See
         sane-bh(5) for details.

       canon
         The canon backend supports the CanoScan 300,  CanoScan  600,  and  CanoScan  2700F  SCSI
         flatbed scanners. See sane-canon(5) for details.

       canon630u
         The   canon630u  backend  supports  the  CanoScan  630u  and  636u  USB  scanners.   See
         sane-canon630u(5) for details.

       canon_dr
         The canon_dr backend supports the  Canon  DR-Series  ADF  SCSI  and  USB  scanners.  See
         sane-canon_dr(5) for details.

       canon_pp
         The canon_pp backend supports the CanoScan FB330P, FB630P, N340P and N640P parallel port
         scanners.  See sane-canon_pp(5) for details.

       cardscan
         This backend provides support for Corex Cardscan USB scanners. See sane-cardscan(5)  for
         details.

       coolscan
         This  is  a  SANE  backend  for  Nikon  Coolscan film-scanners. See sane-coolscan(5) for
         details.

       coolscan2
         This is a SANE backend for  Nikon  Coolscan  film-scanners.   See  sane-coolscan2(5)  or
         http://coolscan2.sourceforge.net for details.

       epjitsu
         The  epjitsu  backend  provides  support  for  Epson-based  Fujitsu  USB  scanners.  See
         sane-epjitsu(5) for details.

       epson
         The SANE epson backend provides support for Epson SCSI, parallel port  and  USB  flatbed
         scanners. See sane-epson(5) for details.

       fujitsu
         The  fujitsu  backend  provides  support  for most Fujitsu SCSI and USB, flatbed and adf
         scanners. See sane-fujitsu(5) for details.

       genesys
         The genesys backend provides support for several scanners based  on  the  Genesys  Logic
         GL646,  GL841,  GL843,  GL847  and  GL124 chips like the Medion 6471 and Hewlett-Packard
         2300c.
          See sane-genesys(5) for details.

       gt68xx
         The gt68xx backend provides support for scanners based  on  the  Grandtech  GT-6801  and
         GT-6816  chips  like  the Artec Ultima 2000 and several Mustek BearPaw CU and TA models.
         Some Genius, Lexmark, Medion, Packard Bell, Plustek, and Trust scanners  are  also  sup‐
         ported. See sane-gt68xx(5) for details.

       hp
         The  SANE  hp  backend provides access to Hewlett-Packard ScanJet scanners which support
         SCL (Scanner Control Language by HP). See sane-hp(5) for details.

       hpsj5s
         The SANE backend for the Hewlett-Packard ScanJet  5S  scanner.  See  sane-hpsj5s(5)  for
         details.

       hp3500
         The  SANE  backend  for  the Hewlett-Packard ScanJet 3500 series. See sane-hp3500(5) for
         details.

       hp3900
         The SANE backend for the Hewlett-Packard ScanJet 3900  series.  See  sane-hp3900(5)  for
         details.

       hp4200
         The  SANE  backend  for  the Hewlett-Packard ScanJet 4200 series. See sane-hp4200(5) for
         details.

       hp5400
         The SANE backend for the Hewlett-Packard ScanJet 54XXC series.  See  sane-hp5400(5)  for
         details.

       hpljm1005
         The  SANE  backend for the Hewlett-Packard LaserJet M1005 scanner. See sane-hpljm1005(5)
         for details.

       hs2p
         The SANE backend for the Ricoh IS450 family  of  SCSI  scanners.  See  sane-hs2p(5)  for
         details.

       ibm
         The SANE backend for some IBM and Ricoh SCSI scanners. See sane-ibm(5) for details.

       kodak
         The SANE backend for some large Kodak scanners. See sane-kodak(5) for details.

       kodakaio
         The SANE backend for Kodak AiO printer/scanners. See sane-kodakaio(5) for details.

       kvs1025
         The SANE backend for Panasonic KV-S102xC scanners. See sane-kvs1025(5) for details.

       leo
         This  backend  supports  the  Leo  S3  and  the Across FS-1130, which is a re-badged LEO
         FS-1130 scanner. See sane-leo(5) for details.

       lexmark
         This backend supports the Lexmark X1100 series of USB scanners. See sane-lexmark(5)  for
         details.

       ma1509
         The  ma1509  backend  supports  the  Mustek  BearPaw  1200F  USB  flatbed  scanner.  See
         sane-ma1509(5) for details.

       magicolor
         The magicolor backend  supports  the  KONICA  MINOLTA  magicolor  1690MF  multi-function
         printer/scanner/fax. See sane-magicolor(5) for details.

       matsushita
         This  backend  supports  some Panasonic KVSS high speed scanners. See sane-matsushita(5)
         for details.

       microtek
         The microtek backend provides access to the "second generation" Microtek  scanners  with
         SCSI-1 command set. See sane-microtek(5) for details.

       microtek2
         The  microtek2  backend  provides access to some Microtek scanners with a SCSI-2 command
         set. See sane-microtek2(5) for details.

       mustek
         The SANE mustek backend supports most Mustek SCSI flatbed scanners including the Paragon
         and  ScanExpress  series  and the 600 II N and 600 II EP (non-SCSI). Some Trust scanners
         are also supported. See sane-mustek(5) for details.

       mustek_pp
         The mustek_pp backend provides access to Mustek  parallel  port  flatbed  scanners.  See
         sane-mustek_pp(5) for details.

       mustek_usb
         The  mustek_usb backend provides access to some Mustek ScanExpress USB flatbed scanners.
         See sane-mustek_usb(5) for details.

       mustek_usb2
         The mustek_usb2 backend provides access to scanners using the  SQ113  chipset  like  the
         Mustek BearPaw 2448 TA Pro USB flatbed scanner. See sane-mustek_usb2(5) for details.

       nec
         The  SANE  nec  backend  supports  the NEC PC-IN500/4C SCSI scanner. See sane-nec(5) for
         details.

       niash
         The niash backend supports the Agfa Snapscan Touch and the HP ScanJet 3300c, 3400c,  and
         4300c USB flatbed scanners. See sane-niash(5) for details.

       p5
         The SANE backend for Primax PagePartner. See sane-p5(5) for details.

       pie
         The  pie  backend  provides  access  to  Pacific Image Electronics (PIE) and Devcom SCSI
         flatbed scanners. See sane-pie(5) for details.

       pixma
         The  pixma  backend  supports  Canon  PIXMA  MP  series  (multi-function  devices).  See
         sane-pixma(5) or http://home.arcor.de/wittawat/pixma/ for details.

       plustek
         The SANE plustek backend supports USB flatbed scanners that use the National Semiconduc‐
         tor LM983[1/2/3] chipset aka Merlin. Scanners using this LM983x chips include some  mod‐
         els  from  Plustek,  KYE/Genius,  Hewlett-Packard,  Mustek,  Umax, Epson, and Canon. See
         sane-plustek(5) for details.

       plustek_pp
         The SANE plustek_pp backend supports Plustek parallel port flatbed  scanners.   Scanners
         using  the Plustek ASIC P96001, P96003, P98001 and P98003 include some models from Plus‐
         tek, KYE/Genius, Primax. See sane-plustek_pp(5) for details.

       ricoh
         The ricoh backend provides access to the following  Ricoh  flatbed  scanners:  IS50  and
         IS60. See sane-ricoh(5) for details.

       s9036
         The  s9036  backend  provides access to Siemens 9036 flatbed scanners. See sane-s9036(5)
         for details.

       sceptre
         The sceptre backend provides access to the Sceptre S1200 flatbed scanner. See sane-scep‐
         tre(5) for details.

       sharp
         The SANE sharp backend supports Sharp SCSI scanners. See sane-sharp(5) for details.

       sm3600
         The  SANE  sm3600  backend  supports  the  Microtek  ScanMaker  3600  USB  scanner.  See
         sane-sm3600(5) for details.

       sm3840
         The SANE  sm3840  backend  supports  the  Microtek  ScanMaker  3840  USB  scanner.   See
         sane-sm3840(5) for details.

       snapscan
         The  snapscan  backend supports AGFA SnapScan flatbed scanners. See sane-snapscan(5) for
         details.

       sp15c
         This  backend  supports  the  Fujitsu  FCPA  ScanPartner  15C   flatbed   scanner.   See
         sane-sp15c(5) for details.

       st400
         The sane-st400 backend provides access to Siemens ST400 and ST800. See sane-st400(5) for
         details.

       tamarack
         The SANE tamarack backend supports Tamarack Artiscan flatbed  scanners.  See  sane-tama‐
         rack(5) for details.

       teco1 teco2 teco3
         The  SANE teco1, teco2 and teco3 backends support some TECO scanners, usually sold under
         the Relisys, Trust, Primax, Piotech, Dextra names. See sane-teco1(5), sane-teco2(5)  and
         sane-teco3(5) for details.

       u12
         The sane-u12 backend provides USB flatbed scanners based on Plustek's ASIC 98003 (paral‐
         lel-port ASIC) and a GeneSys Logics' USB-parport bridge chip like the  Plustek  OpticPro
         U(T)12. See sane-u12(5) for details.

       umax
         The  sane-umax  backend  provides access to several UMAX-SCSI-scanners and some Linotype
         Hell SCSI-scanners. See sane-umax(5) for details.

       umax_pp
         The sane-umax_pp backend provides access to Umax parallel port flatbed scanners and  the
         HP 3200C. See sane-umax_pp(5) for details.

       umax1200u
         The sane-umax1220u backend supports the UMAX Astra 1220U (USB) flatbed scanner (and also
         the UMAX Astra 2000U, sort of). See sane-umax1220u(5) for details.

       Also, have a look at the backend information page at http://www.sane-project.org/sane-sup‐
       ported-devices.html and the list of projects in /usr/share/doc/libsane/PROJECTS.

BACKENDS FOR DIGITAL CAMERAS
       dc210
         Backend for Kodak DC210 Digital Camera. See sane-dc210(5).

       dc240
         Backend for Kodak DC240 Digital Camera. See sane-dc240(5).

       dc25
         Backend for Kodak DC20/DC25 Digital Cameras. See sane-dc25(5).

       dmc
         Backend for the Polaroid Digital Microscope Camera. See sane-dmc(5).

       gphoto2
         Backend   for   digital   cameras  supported  by  the  gphoto2  library  package.   (See
         http://www.gphoto.org for more information and a list of  supported  cameras.)   Gphoto2
         supports  over  140 different camera models.  However, please note that more development
         and testing is needed before all of these cameras will be  supported  by  SANE  backend.
         See sane-gphoto2(5).

       qcam
         Backend for Connectix QuickCam cameras. See sane-qcam(5).

       stv680
         The  sane-st680 backend provides access to webcams with a stv680 chip. See sane-st680(5)
         for details.

       Also, have a look at the backend information page at http://www.sane-project.org/sane-sup‐
       ported-devices.html and the list of projects in /usr/share/doc/libsane/PROJECTS.

MISCELLANEOUS BACKENDS
       dll
         The sane-dll library implements a SANE backend that provides access to an arbitrary num‐
         ber of other SANE backends by dynamic loading. See sane-dll(5).

       net
         The SANE network daemon saned provides access to scanners located on different computers
         in connection with the net backend. See sane-net(5) and saned(8).

       pnm
         PNM  image  reader  pseudo-backend.  The  purpose of this backend is primarily to aid in
         debugging of SANE frontends. See sane-pnm(5).

       pint
         Backend for scanners that use the PINT (Pint Is Not  Twain)  device  driver.   The  PINT
         driver is being actively developed on the OpenBSD platform, and has been ported to a few
         other *nix-like operating systems. See sane-pint(5).

       test
         The SANE test backend is for testing frontends and the SANE installation.   It  provides
         test pictures and various test options. See sane-test(5).

       v4l
         The  sane-v4l  library  implements  a SANE backend that provides generic access to video
         cameras and similar equipment using the V4L (Video for Linux) API. See sane-v4l(5).

       Also, have a look at the backend information page at http://www.sane-project.org/sane-sup‐
       ported-devices.html and the list of projects in /usr/share/doc/libsane/PROJECTS.

CHANGING THE TOP-LEVEL BACKEND
       By  default, all SANE backends (drivers) are loaded dynamically by the sane-dll meta back‐
       end. If you have any questions about the dynamic loading, read sane-dll(5).  SANE frontend
       can  also  be  linked  to  other backends directly by copying or linking a backend to lib‐
       sane.so in /usr/lib/arch_triplet/sane.

DEVELOPER'S DOCUMENTATION
       It's not hard to write a SANE backend. It can take some time,  however.  You  should  have
       basic  knowledge  of  C and enough patience to work through the documentation and find out
       how your scanner works. Appended is a list of some documents that help to  write  backends
       and frontends.

       The SANE standard defines the application programming interface (API) that is used to com‐
       municate between frontends and backends. It can be found at /usr/share/doc/libsane/sane.ps
       (if    latex    is    installed    on    your   system)   and   on   the   SANE   website:
       http://www.sane-project.org/html/ (HTML),  or  http://www.sane-project.org/sane.ps  (Post‐
       script).

       There  is  some  more  information for programmers in /usr/share/doc/libsane/backend-writ‐
       ing.txt.  Most of the  internal  SANE  routines  (sanei)  are  documented  using  doxygen:
       http://www.sane-project.org/sanei/.   Before a new backend or frontend project is started,
       have a look at /usr/share/doc/libsane/PROJECTS for projects that are planned  or  not  yet
       included    into    the    SANE    distribution    and   at   our   bug-tracking   system:
       http://www.http://www.sane-project.org/bugs.html.

       There  are  some  links  on  how  to  find  out  about  the   protocol   of   a   scanner:
       http://www.meier-geinitz.de/sane/misc/develop.html.

       If  you  start writing a backend or frontend or any other part of SANE, please contact the
       sane-devel mailing list for coordination so the same work isn't done twice.

FILES
       /etc/sane.d/*.conf
              The backend configuration files.

       /usr/lib/arch_triplet/sane/libsane-*.a
              The static libraries implementing the backends.

       /usr/lib/arch_triplet/sane/libsane-*.so
              The shared libraries implementing the backends (present  on  systems  that  support
              dynamic loading).

       /usr/share/doc/libsane/*
              SANE documentation: The standard, READMEs, text files for backends etc.

PROBLEMS
       If  your  device  isn't  found  but  you  know  that it is supported, make sure that it is
       detected by your operating system. For SCSI and USB scanners,  use  the  sane-find-scanner
       tool  (see  sane-find-scanner(1)  for details). It prints one line for each scanner it has
       detected and some comments (#). If sane-find-scanner finds your scanner only as  root  but
       not  as  normal  user, the permissions for the device files are not adjusted correctly. If
       the scanner isn't found at all, the operating system hasn't detected it and may need  some
       help.  Depending  on  the type of your scanner, read sane-usb(5) or sane-scsi(5).  If your
       scanner (or other device) is not connected over the SCSI bus or USB,  read  the  backend's
       manual page for details on how to set it up.

       Now  your  scanner is detected by the operating system but not by SANE?  Try scanimage -L.
       If  the  scanner  is  not  found,  check  that  the  backend's  name   is   mentioned   in
       /etc/sane.d/dll.conf.  Some backends are commented out by default. Remove the comment sign
       for your backend in this case. Also some backends aren't compiled at all if one  of  their
       prerequisites are missing. Examples include dc210, dc240, canon_pp, hpsj5s, gphoto2, pint,
       qcam, v4l, net, sm3600, snapscan, pnm. If you need one of these backends and  they  aren't
       available,  read the build instructions in the README file and the individual manual pages
       of the backends.

       Another reason for not being detected by scanimage -L may be a missing or wrong configura‐
       tion  in  the  backend's  configuration  file. While SANE tries to automatically find most
       scanners, some can't be setup correctly without the  intervention  of  the  administrator.
       Also  on  some  operating  systems auto-detection may not work. Check the backend's manual
       page for details.

       If your scanner is still not found, try setting the various environment variables that are
       available  to  assist in debugging.  The environment variables are documented in the rele‐
       vant manual pages.  For example, to get the maximum amount of debug information when test‐
       ing  a  Mustek  SCSI scanner, set environment variables SANE_DEBUG_DLL, SANE_DEBUG_MUSTEK,
       and SANE_DEBUG_SANEI_SCSI to 128 and then invoke scanimage -L .  The  debug  messages  for
       the  dll  backend  tell if the mustek backend was found and loaded at all. The mustek mes‐
       sages explain what the mustek backend is doing while the  SCSI  debugging  shows  the  low
       level  handling. If you can't find out what's going on by checking the messages carefully,
       contact the sane-devel mailing list for help (see REPORTING BUGS below).

       Now that your scanner is found by scanimage -L, try to do a  scan:  scanimage  >image.pnm.
       This  command  starts a scan for the default scanner with default settings. All the avail‐
       able options are listed by running scanimage --help.  If scanning  aborts  with  an  error
       message,  turn  on  debugging  as mentioned above. Maybe the configuration file needs some
       tuning, e.g. to setup the path to a firmware that is needed  by  some  scanners.  See  the
       backend's manual page for details. If you can't find out what's wrong, contact sane-devel.

       To  check  that  the  SANE libraries are installed correctly you can use the test backend,
       even if you don't have a scanner or other SANE device:

              scanimage -d test -T

       You should get a list of PASSed tests. You can do the same with your backend  by  changing
       "test" to your backend's name.

       So  now  scanning  with scanimage works and you want to use one of the graphical frontends
       like xsane, xscanimage, or quiteinsane but those frontends don't detect your scanner?  One
       reason  may  be  that  you  installed  two  versions  of  SANE.  E.g. the version that was
       installed by your distribution in /usr and one you installed from source  in  /usr/local/.
       Make  sure  that only one version is installed. Another possible reason is, that your sys‐
       tem's  dynamic  loader  can't  find  the  SANE  libraries.  For  Linux,  make  sure   that
       /etc/ld.so.conf  contains  /usr/local/lib  and  does not contain /usr/local/lib/sane.  See
       also the documentation of the frontends.

HOW CAN YOU HELP SANE
       We appreciate any help we can get. Please have a look at our web page  about  contributing
       to SANE: http://www.sane-project.org/contrib.html

CONTACT
       For  reporting  bugs  or  requesting  new  features,  please  use our bug-tracking system:
       http://www.sane-project.org/bugs.html.  You can also contact the author  of  your  backend
       directly.  Usually  the  email  address can be found in the /usr/share/doc/libsane/AUTHORS
       file or the backend's manpage. For general discussion about  SANE,  please  use  the  SANE
       mailing list sane-devel (see http://www.sane-project.org/mailing-lists.html for details).

SEE ALSO
       saned(8),    sane-find-scanner(1),    scanimage(1),   sane-abaton(5),   sane-agfafocus(5),
       sane-apple(5),  sane-artec(5),  sane-artec_eplus48u(5),   sane-as6e(5),   sane-avision(5),
       sane-bh(5),    sane-canon(5),   sane-canon630u(5),   sane-canon_dr(5),   sane-canon_pp(5),
       sane-cardscan(5),  sane-coolscan2(5),  sane-coolscan(5),   sane-dc210(5),   sane-dc240(5),
       sane-dc25(5),  sane-dll(5),  sane-dmc(5), sane-epson(5), sane-fujitsu(5), sane-genesys(5),
       sane-gphoto2(5),    sane-gt68xx(5),    sane-hp(5),     sane-hpsj5s(5),     sane-hp3500(5),
       sane-hp3900(5),    sane-hp4200(5),    sane-hp5400(5),    sane-hpljm1005(5),   sane-ibm(5),
       sane-kodak(5),   sane-leo(5),   sane-lexmark(5),    sane-ma1509(5),    sane-matsushita(5),
       sane-microtek2(5),        sane-microtek(5),       sane-mustek(5),       sane-mustek_pp(5),
       sane-mustek_usb(5),   sane-mustek_usb2(5),   sane-nec(5),   sane-net(5),    sane-niash(5),
       sane-pie(5), sane-pint(5), sane-plustek(5), sane-plustek_pp(5), sane-pnm(5), sane-qcam(5),
       sane-ricoh(5),    sane-s9036(5),     sane-sceptre(5),     sane-scsi(5),     sane-sharp(5),
       sane-sm3600(5),    sane-sm3840(5),    sane-snapscan(5),    sane-sp15c(5),   sane-st400(5),
       sane-stv680(5),    sane-tamarack(5),    sane-teco1(5),    sane-teco2(5),    sane-teco3(5),
       sane-test(5),  sane-u12(5), sane-umax1220u(5), sane-umax(5), sane-umax_pp(5), sane-usb(5),
       sane-v4l(5)

AUTHOR
       David Mosberger-Tang and many many more (see /usr/share/doc/libsane/AUTHORS for  details).
       This man page was written by Henning Meier-Geinitz. Quite a lot of text was taken from the
       SANE standard, several man pages, and README files.

                                           14 Jul 2008                                    sane(7)
