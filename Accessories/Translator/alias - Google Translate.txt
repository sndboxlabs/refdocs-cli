#Usage: translate <phrase> <output-language> Example: translate "Bonjour! Ca va?" en 

#See this for a list of language codes: http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes

#Add this to your .bashrc file:

function translate(){ wget -U "Mozilla/5.0" -qO - "http://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=$2&dt=t&q=$(echo $1 | sed "s/[\"'<>]//g")" | sed "s/,,,0]],,.*//g" | awk -F'"' '{print $2, $6}'; }
