From: http://members.iinet.net.au/~jimshaw/yabasic.me.uk/primers/two/default.htm

#####[Yabasic: Primer 2]#####

 

Welcome back. In the last primer we looked at drawing 2D. In this primer we
will look at how to do color and how the “goto” stuff, shown in primer 1,
works.

 

| Colors
`-------
 

First let’s look at colors. I’ll explain how colors work then tie it in to
what we did in primer 1.

 

Colors are really easy to do in YABASIC.  To color in an object like a
rectangle or circle you use this line of code:

 

┌───────────────────────┐
│                       │
│                       │
│Setrgb 1,val,val,val   │
└───────────────────────┘


The 1 tells the program you are going to colour in an object on the screen.

 

The values after the 1 determine the colour. The first val is the amount of
red. Make it a high number and you get a lot of red

 
┌────────────────┐
│                │
│                │
│Setrgb 1,255,0,0│
└────────────────┘


This makes red.

 

The second val is the amount of green and the third one is the amount of blue.
(Hence the setrgb that means set up red, green and blue values)

 

Here are the main colors:

 

┌────────────────────────────────┐
│                                │
│                                │
│Setrgb 1,255,0,0      = red     │
│                                │
│Setrgb 1,0,255,0      = green   │
│                                │
│Setrgb 1,0,0,255      = blue    │
│                                │
│Setrgb 1,255,255,255  = white   │
│                                │
│                                │
└────────────────────────────────┘

 

 

You can make lots of colors by change the values of red, green and blue.

 

Now let’s tie this in with primer 1. We learned last time that :

 

┌──────────────────────────────────┐
│                                  │
│                                  │
│open window 640,512               │
│                                  │
│label example                     │
│                                  │
│fill rectangle 100,200 to 500,400 │
│                                  │
│goto example                      │
│                                  │
│                                  │
└──────────────────────────────────┘


draws a gray rectangle. To add some color in we add the setrgb line before the
shape command

 

┌──────────────────────────────────┐
│                                  │
│                                  │
│open window 640,512               │
│                                  │
│label example                     │
│                                  │
│setrgb 1,255,255,255              │
│                                  │
│fill rectangle 100,200 to 500,400 │
│                                  │
│goto example                      │
│                                  │
│                                  │
└──────────────────────────────────┘


The above example makes the rectangle white.

 

This one draws a red circle:

┌─────────────────────────┐
│                         │
│                         │
│open window 640,512      │
│                         │
│label example2           │
│                         │
│setrgb 1,255,0,0         │
│                         │
│fill circle 100,100,50   │
│                         │
│goto example2            │
│                         │
│                         │
└─────────────────────────┘

 
And that is all that is needed to color in your shapes.


Now we will learn what all this “goto” stuff means. Look at this example:


┌─────────────────────────┐
│                         │
│                         │
│open window 640,512      │
│                         │
│label circ               │
│                         │
│fill circle 10,20,50     │
│                         │
│goto circ                │
│                         │
│                         │
└─────────────────────────┘

 

Now this is how it works. The program opens the window, draws the circle and
then goes back to the beginning by using the "goto" command. The code then
searches for what is after the “goto” – in this case it is circ but you can
call it whatever you like. Label is just a way of highlighting to the program
this is where the circ code starts. “Goto” searches for “Label” and if what is
after the goto matches what is after the label then the program runs from the
label.  The program above runs in a continuous loop as when it reaches the
bottom it jumps back up to the “label” and draws the circle again.

 

Hopefully that has now tied up everything from primer 1 and you can now draw
shapes of different sizes and color them in.

 

In primer 3 we are going to move on and discover what variables are and how
using them makes life really easy.

 

I’ll see you in primer 3!

 

 

