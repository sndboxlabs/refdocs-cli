                                Optimiz7.Txt

			       by  Danny Gump
			danielgump@chainmailsales.com
		    http://chainmailsales.com/virtuasoft

        This text file is the eighth in a guide for use by QB/QBASIC
programmers to help optimize the program code for greater efficiency and/or
give a program a more professional look..  If you have any questions, contact
VirtuaSoft at gump@gnt.net.  Address to Danny.

        (Note: This is not for the beginning programmer.  A strong
        background in QB/QBASIC programming is highly recommended.)

8. BSAVE and BLOAD

        BLOAD and BSAVE directly address memory, unlike OPEN, so you need to 
    define the starting location in memory (segment and offset) where the file
    will start reading or writing.  To BSAVE, you also need to tell how large
    the file will be in bytes.  The syntax for BSAVE and BLOAD are set up as
    follows: 

            DEF SEG = Segment
		BSAVE "FileName", Offset, NumBytes 

            DEF SEG = Segment
		BLOAD "FileName", Offset 

        (NumBytes counts 0, so subtract 1 byte from whatever your total size is.) 
        To save or load an image from the graphics screen, use the segment &ha000
    and the offset 0; for text screens, use &hb800 and 0; and for arrays, use
    VARSEG (array%(0)) and VARPTR(array%(0)).


	BLOAD and BSAVE are great for their lightning-quick file accessing, but
    that comes at a penalty: ease of use.  Thus, you must know exactly what you
    are doing before you even try to use them.  Following are some common causes
    of BLOAD- and BSAVE-related crashes in the computer:

        *A file was loaded into an array that was too small.  BLOAD doesn't care
        	what's in memory.  It will continue writing until it's done.
		Make sure all arrays can accomidate the file size.

        *The file will not load properly into an array.  You need to make sure
		you give the correct offset in BLOAD.  If the array has more
        	than 1 element (element 0), you need to use VARPTR(array(0)).
		(Also, DEF SEG must be set to element 0.)  If the array has
		more than on dimension, be sure to take that into account in
		BLOAD.  If the array has no DIMmed elements, there's no need
		to do any more than VARPTR(array) in BLOAD.

	*The file won't save properly.  As explained above, make sure you gave
		the proper segment and offset for the array.


               ... This concludes the lesson ...
