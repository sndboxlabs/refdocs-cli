<html>
<head>
<title>The Game Design Process by Francois Dominic Laramee</title>
</head>

<body>

<h1>
<font color="#222222">The Game Design Process</font></h1>
There is no such thing as a "standard" game design document, and for good
reason.&nbsp; It would be insane to try to fit an interactive fiction adventure
and a Tetris-like puzzle into the same mold.&nbsp; However, all forms of
entertainment do have a certain number of characteristics in common, and
over the years, I have developed a methodology which provides structure
to my game design activities and help focus my work.&nbsp; This page describes,
in a nutshell, how I start with an idea and develop it into a finished
product which can be handed over to a producer and turned into a game.
<p>Some of the issues I will talk about here I have learned the hard way
(i.e., by discovering late in development that a crucial point had not
been thought out well), others I have gleaned from a variety of sources
and integrated into my own routine.&nbsp; I have had the chance to work
with people who had backgrounds in film, animation, sales and marketing
in addition to games, and my design has improved a great deal thanks to
them.&nbsp; I recommend that you take the time to look at <a href="http://www.digitalarcana.com">Digital
Arcana</a>'s methodology (which has strong film-making influences) and
at Ben Sawyer's <i>Ultimate Game Developer's Sourcebook</i> (ISBN: 1-883577-59-4)
for very good discussions of the game design process.

<p>Note that, while I write with computer game design in mind (being that
this is what I usually do), many elements also apply to board, role-playing,
or play-by-email design.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">Game Design in Six Easy Steps</font></h2>
Depending on the type of game you are creating and on how you expect to
get it produced, you may want to develop as many as six different documents
at various stages of the creative process.&nbsp; If it sounds like a lot
of work, then you are beginning to get my point ;)
<ul>
<li>
All games should begin with a <b>design treatment</b>, i.e., a quick discussion
of your product's unique features and target audience.</li>

<li>

Then, you should move to a <b>preliminary design</b>, discussing the game's
rules, content and behaviour in a purely qualitative way.&nbsp; This document
should be circulated and discussed as widely as possible given the situation.</li>

<li>
A <b>final design</b> is a re-write of the previous document, which etches
the product's features in stone.</li>

<li>
The <b>product specification</b> (which only really makes sense for interactive
products) details how the features adopted in the final design will be
implemented.</li>

<li>
The <b>graphic bible</b> determines the look and feel of the game's characters,
maps, props, etc.</li>

<li>
The <b>interactive screenplay</b>, if appropriate, contains the dialogs
and the storyline implemented into the product.</li>
</ul>
The game designer may or may not be qualified to write all of these documents.&nbsp;
A product specification for a computer game, for example, requires considerable
input on the part of the game's producer, lead programmer and lead artist,
while a screenplay should be crafted by a professional writer.&nbsp; However,
as "guardian of the vision", the designer should have final say on what
goes into his product, and be involved in all aspects of the process, if
only as an overseer.

<p>A word to the wise: <i>as long as you are still in design, it is never
too late to cancel a project</i>.&nbsp; Even if you have a final design
document and a full screenplay in hand and are ready to turn your creation
over to a producer, you have probably spent less than 10% of your total
production budget.&nbsp; Unless everyone in the company (studio staff,
sales, press relations, everyone) is convinced that the game is going to
be a winner, pull the plug now.&nbsp; Six months from now, when you have
a million dollars committed, it will be too late.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">The Design Treatment</font></h2>
I have seen several different definitions of the "design treatment", "design
proposal", "game outline" or "spec sheet".&nbsp; Mine is more or less a
middle ground between all of them.

<p>The design treatment is a short (4-8 pages) description of what kind
of product you would like to do and what you would need to do it.&nbsp;
The document's purposes are multiple: it helps the designer to structure
the "creative spark" into something tangible that can be discussed, it
establishes a framework for future design work, and it serves as a marketing
tool to generate interest in the product in company executives and/or publishers.
<p>The exact content of a design treatment may vary according to the intended
audience, but not by much.&nbsp; Financial information may not be appropriate
for a first contact with a publisher, but otherwise, everyone who you will
want to submit your treatment to will need to see the following:
<ul>
<li>
The game's genre and key features.&nbsp; For example, a design treatment
for Doom might have read: a shooting game, with a first-person perspective
based on a 3D ray-casting engine.</li>

<li>
A short description of the experience you want to give to the player.&nbsp;
Is the game a comedy?&nbsp; An adrenaline thriller?&nbsp; A puzzle?&nbsp;

Will it be a technology showcase, a graphic wonder or a story?&nbsp; Linear
or not?&nbsp; Is it single-player or multi-player?&nbsp; Playable over
a LAN or the internet?&nbsp; What are the key elements of gameplay and
of the interface?</li>

<li>
What kind of look-and-feel do you want?&nbsp; A realistic 3D style?&nbsp;
Cartoons?&nbsp; Isometric?&nbsp; Lots of intricate move combinations and
secret rooms, or straightforward gameplay suitable for children?</li>

<li>
How will you structure the game: levels?&nbsp; Missions?&nbsp; Episodes?&nbsp;
How many?&nbsp; How long will each be?&nbsp; How will you provide replay
value?</li>

<li>
What resources will you need?&nbsp; How many people will work on the game,
for how long?&nbsp; Do you need senior engineers with knowledge of 3D,
of network programming, of Artificial Intelligence?&nbsp; (Do you have
them on staff, and if you don't, can you hire them?)</li>

<li>
Will you need an engine?&nbsp; New development tools (i.e., a new version
of Softimage 3D)?&nbsp; Will you buy, or develop in-house?</li>

<li>
Will you acquire a license to some well-established intellectual property,
or create your own?&nbsp; The right license at the right time can mean
"free" extra sales, but it can be expensive.&nbsp; And you must buy now
a license that will have value in two years; Looney Tunes are a safe bet,
the Macarena is not.</li>

<li>
Who are your major characters/units/game pieces?</li>

<li>
Who is your target audience?&nbsp; A trivia game, a Barbie clothing designer,
a chess engine and a Quake killer will appeal to very different people.</li>

<li>
What platform(s) will you develop for?&nbsp; Which will come first?&nbsp;
Will you port the game to other platforms yourself, or hire sub-contractors
to do it for you?</li>

<li>
How is your team qualified to deliver this product?&nbsp; Industry experience
and a track record make publishers relax, but even if you are relative
beginners, you may have more to sell than you think.&nbsp; If your brother-in-law
is the pit crew chief for last year's Winston Cup winner and he agrees
to consult on your NASCAR racing game, you have a winner.&nbsp; Same thing
if you are working on a simulation of the Crusades and you can bring a
historian from a local college on board.</li>

<li>
A preliminary estimate of the budget and production schedule.&nbsp; At
this stage, something as vague as "about 10-12 months, for a team of 6;
will cost 500K to 750K with tools and fixed costs" will be enough; no one
expects a precise budget from a five-pager, but the reader will need to
know whether this is going to be a tight-budget production or a main-eventer.</li>
</ul>

As the design treatment is your project's business card, it should be short,
factual and representative.&nbsp; If you have some preliminary sketch art
available (i.e., your main characters and a scene or two), by all means,
include it.&nbsp; If your game is going to be a comedy, the text should
be funny.&nbsp; And unless you have a compelling reason to do otherwise,
limit the document's size to 4-5 pages of text, plus 2-3 pages of art if
available; publishers receive hundreds of these documents, so anything
longer may not be read at all.
<p>I have posted a sample design treatment on my page; you can find it
<a href="http://pages.infinit.net/idjy/games/wheel.html">here</a>.
<p>All in all, a design treatment should not require more than 100 to 150
person/hours of work; 75 if it doesn't include any graphics.&nbsp; If you
put more work than that into it, you are wasting your time.&nbsp; If you
are an amateur designing for fun, then it's no big problem, but if you
work for a professional game development house, these 100-150 hours you
have already put in probably cost the company 5K to 7K$ in various expenses.&nbsp;
Since odds are that your treatment will never lead to a full design (more
on that later), it's enough.&nbsp; If your creative juices are still flowing,
go work on another treatment.

<br>&nbsp;
<h2>
<font color="#222222">A few words of caution</font></h2>
All done?&nbsp; Your treatment is finished, and you believe that you are
sitting on a gold mine?
<p>Good.&nbsp; Stay seated for a while.
<p>Stuff your treatment in a drawer, take a deep breath, go bowling, call
your aunt Maria in Portugal, and do not think about your great idea for,
oh, at least two weeks.&nbsp; Then, if you still think it's great, go ahead
and send it out in the world.
<p>Nothing kills so many game development companies as two-in-the-morning
ideas that get implemented just because no one stopped to look them over
and realize that they stunk.&nbsp; As an executive for a small computer
game publisher, I saw tens of finished or highly advanced products that
should never, ever have gone beyond the treatment stage.&nbsp; I remember
meeting with a group of young hopefuls at the 1997 CGDC; the game they
had worked on for months and months was technically sound, visually adequate
and easy enough to play, but it was based on an idea so ludicrously bad
that it didn't deserve a shareware release, much less the million-dollar
contract these guys were looking for.&nbsp; There wasn't five minutes of
gameplay in there.&nbsp; They wasted their time.&nbsp; And the worst thing
is, their production values showed that these guys had talent; with the
right design, they could have made it.&nbsp; I hope they will, someday.

<p>Circulate your design treatment as widely as possible, and if it generates
some interest, then (and only then) go on to a full design and a demo.&nbsp;
Sure, you run the risk that your ideas will get stolen; it happens all
the time.&nbsp; However, there is not a whole lot you can do about it,
unless you have the funds (or the spare time) to work on a complete design
and a working demo before you start looking for a publisher.&nbsp; And
that is the second most-popular reason why development studios fail: they
bet the farm on a product and then find no channel to market.&nbsp; If
at all possible, walk in small steps.
<br>&nbsp;
<h2>
<font color="#222222">Game Development Food Chain</font></h2>
The smart designer never gets too attached to his ideas.&nbsp; The reason
why can be summarized as follows:
<ul>

<li>
Out of 10 design treatments written and submitted, maybe 3 or 4 will attract
enough interest to warrant a preliminary design, and 2-3, a complete design.</li>

<li>
Out of 3 final designs, it is likely that 1 will not be produced at all,
or that it's production will be cancelled midway because of lack of funding.</li>

<li>
Out of 10 completed computer games, 2-3 will get wide distribution, 4-6
will get more modest market penetration, and some will never be distributed
at all.&nbsp; Publishers break contracts when they run into financial trouble;
if you are (very) lucky, the advances they paid will allow you to survive
and develop another game.</li>

<li>
Out of the 1,200 to 1,600 PC games released every year, it is estimated
that fewer than 100 break even financially, and only a few dozen really
make significant money.</li>

</ul>
The situation is a bit rosier in the game console market, but you get the
point: smash hits are few and far between, and not necessarily because
of flawed designs.&nbsp; Even if you have really great ideas, they may
not turn into a best-seller, because of a problem somewhere in the distribution
channel.&nbsp; If you and your team make good livings and enjoy your work,
be content; that's more than many of your peers are able to say.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">The Preliminary Design Document</font></h2>
A preliminary design document can be thought of as an organized list of
features.&nbsp; It describes what you want your product to offer in terms
of gameplay, technology and look, without worrying too much about how it
will be implemented.&nbsp; (Of course, if you <i>know </i>that something
is impossible, save everyone useless aggravation and take it out now!)

<p>The preliminary design is a discussion document, and it may (should?)
go through several iterations.&nbsp; If you are writing a role-playing
or board game, you can start play-testing at this stage, which will help
you weed out elements that don't work.&nbsp; Computer games are a little
trickier, since you have no software to play with until much later in the
production process; nevertheless, discussing the design document as openly
as possible, with other designers in your company and with lead artists
and programmers, will serve much the same purpose.
<p>The content of a PDD depends so heavily on the type of game being considered
that it is not even worth trying to define a standard.&nbsp; As an example,
here is a partial list of contents for a 3D PlayStation game I once co-wrote:
<ul>
<li>
Technical specifications: frame rate, texture resolution and color depth,
number of player characters, single and multi-player modes, camera handling,
etc.</li>

<li>
Backstory, including a storyboard for the game's FMV intro.</li>

<li>
Cast of characters: the player characters and their unique talents, the
villain, his ships, the supporting cast, etc.</li>

<li>
List of the game's environments and the missions taking place in each.&nbsp;
About 1-2 pages of general information on the unique characteristics of
each mission (i.e., slippery surfaces, low visibility, types of monsters,
race vs shooting, etc.)</li>

<li>
Special ammunition, power-ups, traps, bombs, equipment.</li>

<li>
Monsters and the statistics that define them: endurance, hit potential,
type of behaviour.</li>

<li>
Lives, health, resurrections and tagging between player characters.</li>

<li>
List of moves, including the secret and special-purpose moves appearing
in specific situations.</li>
</ul>
I have seen preliminary designs ranging in size between 20 pages (for a
shooter) and 60 pages (for a very detailed strategic sports simulation.)
<p>While great care should be taken not to waste time in endless discussions,
when in doubt, keep talking.&nbsp; Cutting design time short to save money
is a sure-fire way to run a production into a wall.&nbsp; Preliminary design
can take anywhere between 4 and 10 weeks for the designer, and 10 to 30
hours for the other people involved in brainstorming.
<p>I have posted the revised design document (an advanced form of the preliminary
design) for a pro-wrestling simulation play-by-email game <a href="http://pages.infinit.net/idjy/games/mers.html">here</a>.&nbsp;

Of course, it is a very simple game, and the document for a commercial
computer game would be a lot more complicated, but the level of detail
and the completeness, given the subject matter, is about right.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">The Final Design Document</font></h2>
Discussions on the preliminary design may lead you to drop some features,
scale back others, and introduce new ones.&nbsp; Once everyone agrees on
the functionality the game is going to implement, the design document is
ready to go Final.
<p>A good final design document goes into as much detail as possible.&nbsp;
If you are in doubt as to whether a certain bit of information should be
in there, then it should.&nbsp; EVERYTHING that you can put in is relevant,
because it forces you to think your design through before production begins,
and nothing helps smooth a two-year development cycle as much as a design
which leaves no (or few) crucial decisions to make midway.
<p>There can be a certain amount of overlap between the contents of a final
design and those of a product specification.&nbsp; If the designer happens
to be a senior programmer, for example, he should write down the algorithms
for some of the unique software features he wants in the game; after all,
he thought of them, and no one else knows what behaviour he wants the software
to exhibit more than he does.&nbsp; Something that definitely should be
in the FDD (although I have rarely seen it) is a list of priorities: what
features the game can not live without, what would be really great to have,
and what will be added at the last minute if there is time to spare (or
delayed until the sequel if there is not).&nbsp; It is a fact of life that
some productions go wrong; the game industry is not very strong on software
engineering techniques, and even highly organized studios can get in trouble
if key people leave at the wrong time.&nbsp;&nbsp; A lot of trouble can
be avoided if you know in advance what should be cut if you risk not being
able to deliver in time for Christmas; besides, such "risky" stuff can
be scheduled for later, so that if it needs to be taken out, it won't have
wasted your team's time and effort.

<p>By the time the Final design document is deposed, the lead designer
can have spent 150 to 400 hours on his product, or even more.&nbsp; Final
design documents can range in size between 75 pages (for simple action
games, when most of the level design is written down in a Graphic Bible
or handled informally) and 200 pages or more.&nbsp; My personal record
is around 160.&nbsp; Interactive movies have screenplays that occupy 300-500
pages by themselves!
<p>All the Final design documents I have ever written are either subject
to non-disclosure agreements from former employers and clients, or lost
in the dawn of time.&nbsp; Therefore, I can not post any here.&nbsp; However,
just to give you an idea of how much detail you should put in, here is
a partial table of contents for a typical play-by-mail multi-player sports
simulations:
<ul>
<li>
The length and timing of seasons (how many per year, when do new leagues
open their doors, etc.)</li>

<li>
The hierarchy of leagues necessary to support thousands of teams, with
promotions/demotions between leagues of different levels for strong/weak
teams.</li>

<li>
The procedure to create a new team: standard and custom team colors, creation
of new players, stadium building, expansion draft.</li>

<li>
The database of fantasy player attributes and statistics: over 40 attributes
covering talent (current and potential), health, luck and personality;
a list of every statistic which will be compiled by the system and of the
record books which will be maintained.</li>

<li>
Scouting process: what is visible to the players (and how clearly), and
what remains hidden in the database.</li>

<li>

The reports which will be sent to players: contents, layouts, frequency.</li>

<li>
Detailed description of the data-entry application's interface, including
a list of menus and dialog boxes.</li>

<li>
Franchise management: hiring and firing players, recruiting minor leaguers,
training, trades and trade adjudication mechanism (to prevent one-sided
trades from boosting a team unduly), scouting, ticket prices, salary cap,
contract offers, how to generate revenue from TV and from press releases,
etc.</li>

<li>
Game management options: what orders the player will be able to send, and
how mistakes in player commands will be handled.</li>

<li>
The manual which will be sent to players at registration..</li>

<li>
The game's simulator algorithms, in great detail (including the effects
of weather, fatigue, injuries, etc.)</li>

<li>
Random events: accidents, "real-life" problems influencing fantasy player
performance, grudges, etc.</li>

<li>
Game Definition Language: allows to store games in compact form and to
generate play-by-play text (or audio) files automatically.</li>

<li>
A discussion of game pricing, marketing strategy, and prizes.</li>
</ul>

<hr WIDTH="100%">
<h2>
<font color="#222222">The Product Specification</font></h2>
Once the design itself is over, it is time to move to pre-production.&nbsp;
In this step, the designer's job is to work with the producer, the lead
artist and the lead programmer to ensure that the project plan being developed
will support his vision for the product.
<p>This may mean sitting down with the lead artist to sketch the main characters,
spending time with the lead programmer to make sure that the algorithm
which manages transfer of information between non-player characters will
produce the appropriate behaviour (i.e., let some characters place variable
levels of trust in what they hear from others), making sure that the proper
development tools will be ordered at the appropriate time, etc.
<p>The product specification itself is a blueprint for the development
process.&nbsp; In theory, the designer could leave the team once the product
spec is written, and everything would be fine.&nbsp; Although things are
never this simple in reality, every effort should be made to ensure that
the product specification is as thorough and realistic as possible, because
any mistake can result in a delay of final delivery, extra costs, and extra
overtime in the last months of production.
<p>The product spec should contain the following:
<ul>
<li>

A list of the sound effects and music tracks required in the game.</li>

<li>
A list of the animations, 3D models, textures and other graphics which
need to be produced, in as much detail as possible.&nbsp; If you can list
the exact "idle animations" which will be attached to your main character
at various points in the game, do it.&nbsp; If not, at least decide how
many there will be.&nbsp; The lead artist should estimate the amount of
effort required for each element of content.</li>

<li>
A list of the algorithms which must be developed to implement/upgrade the
game engine and in-house tools.&nbsp; The lead programmer should estimate
the effort required for each major feature.</li>

<li>
A list of all other materials which must be produced by the team: press
materials, demos, screen shots, box art, manual, etc.</li>

<li>
A detailed project plan and schedule, including a preliminary assignment
list for each member of the production team, a list of dependencies (i.e.,
a Pert chart), reasonable milestones, and contingency plans.</li>

<li>
A detailed production budget.&nbsp; Knowing who will work on the project
for how long and what expenses must be made to equip the team, the producer
is now in a position to pinpoint the expected cost of the product, and
to re-arrange priorities accordingly.&nbsp; Very expensive subsidiary features
can be assigned lower priority, so that they can be dropped (before being
spent upon) in case of an emergency.</li>
</ul>

A producer should devote 2-4 weeks to building the list of materials for
a project, and another week to prepare a preliminary schedule.&nbsp; The
designer should be ready to spend the equivalent of 2 weeks of his time
supporting this effort.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">The Graphic Bible</font></h2>
The graphic bible is a common animation industry tool which the interactive
entertainment world would be wise to adopt.&nbsp; When we started introducing
graphic bibles into our design document packages and submitting them to
publishers, back in 1996, they were considered a novelty and attracted
quite a few compliments.
<p>Basically, a graphic bible defines the look of your product.&nbsp; It
contains the following:
<ul>
<li>
Style sheets and color schemes (i.e., pantone) for your characters and
monsters.&nbsp; These may specify 4-10 different views of each character,
which will guide animators and 3D modellers in maintaining consistency
throughout the product; they allow you to let more than one artist work
on the same character at the same time if necessary.</li>

<li>
Style sheets for the major objects, vehicles and props.</li>

<li>
Maps of the levels and environments.&nbsp; This may include the graphical
part of level design.</li>

<li>
Background drawings.</li>

<li>
A storyboard for the intro and for any other FMV/animation sequences in
the product.</li>

</ul>
A designer who happens to be an artist can produce the graphic bible, alone
or in collaboration with others.&nbsp; (It is a <i><u>lot</u></i> of work.)&nbsp;
Designers without an artistic background should still collaborate and supervise
this effort, so as to ensure that the look of the game will be consistent
with their vision.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">The Screenplay</font></h2>
An interactive screenplay is just like an ordinary movie screenplay, except
that instead of a linear list of scenes, you (or the writer you hire) will
have to write independent bits and pieces which can be played in a potentially
large variety of sequences.&nbsp; Not only does this make interactive screenwriting
very complicated (as it is both imperative and excruciating to ensure consistency),
it also makes it long: a typical screenplay for a two-hour interactive
movie contains about six hours of material.&nbsp; Writing an interactive
movie typically requires 4 to 6 months of work, depending on the author's
level of expertise with the medium.

<p>As far as I know, there are still no good tools on the market to facilitate
non-linear writing.&nbsp; Some packages support simple branching stories,
but that's it.&nbsp; For anything more complicated than that, you are on
your own.
<p>Not very many people can write a convincing interactive screenplay,
or explain how to do it.&nbsp; I recommend you look up Lee Sheldon's writings
on Gamasutra or his CGDC tutorials if you are interested in this topic;
he is one of very few masters of the genre.
<p>
<hr WIDTH="100%">
<h2>
<font color="#222222">During Production</font></h2>
Once production is under way, it will be the producer's responsibility
to update the product spec during development, to keep track of the project's
progress, re-assign duties to meet important deadlines, etc.&nbsp;&nbsp;
The designer's job will be to make sure that what gets done is satisfactory,
and that the producer has all the appropriate information to make his decisions.
<p>Of course, this is a lot easier if the designer and the producer are
one and the same; however, the skills required for the two positions are
quite different, and not everyone can excel at both.&nbsp; While a good
designer/producer may be the single most effective person a game studio
can hire, a good team can perform just as well, assuming that both members
can keep their egos in check and work collaboratively.

<p>Unless the designer is also the producer, lead artist or lead programmer,
he should not be busy full-time on a project which is in production.&nbsp;
This should leave him with plenty of time to start working on the team's
next project!
<p>
<hr WIDTH="100%">
<br>&nbsp;
<center>
<address>
By <a href="mailto:francoislaramee@videotron.ca">Francois Dominic Laramee</a></address></center>

</body>
</html>