'___
' |he Code Post 
' `-' Original Submission
' ===============================================================
' CONTRIBUTOR: JMB
' DESCRIPTION: How to save and load screen colors
' DATE POSTED: Thu Jan 10 23:36:31 2002
' ===============================================================

Hm, are you using SCREEN 13? If not, I just wasted a lot of time typing this, 
but oh well... Here it is anyway =) If you'd like to skip the explanation, 
which I do not advise, just go directly to the code- use the routines I show 
you, and you can easily store colours to a file, load colours from a file, 
and modify the colours.

If you use SCREEN 13, you are actually accessing a video mode IBM created 
called VGA (Video Graphics Array). With it, a programmer can use 256 colours, 
which you probably have PSET before. But that is misleading... The VGA card 
can actually produce roughly 256K (256,000) colours. Because of memory and 
speed restrictions at the time it was introduced, IBM needed a way to display 
a given amount of colours. So the 256 values mentioned before actually 
translate into a color palette, a table of colour values that are created 
using Red, Green, and Blue components. (RGB). Obviously, we've come a long 
way from that, because we can now display things in 32bit colour rather than 
a very limited 8bit colour, values ranging from 0 to 255.

Anyway... This palette translation is done with the VGA's Digital-to-Analog 
(DAC) converter. It has 256 entries in a table, each one defining its own 
colour, and each colour consists of 3 parts (RGB). The DAC on VGA cards is 
only 6-bits wide, so the components can also be only 6-bits wide- this means 
that you have only 64 levels of brightness, from 0 to 63.

The OUT and INP commands you see is the way QB programmers talk to the DAC. 
They send information to a "port" on the video card in order to change or 
read colour information.

Changing a color
-----------------
If you OUT to port &H3C8, you can tell the VGA card which colour you want to 
change. Then OUT to port &H3C9 three times, where the first is the Red value, 
second is the Green value, and third is the Blue value. There you go! You 
just changed the RGB values for that colour. =) Here is a short example to 
change the RGB values for a colour:

OUT &H3C8, c% ' colour you want to change, 0 to 255
OUT &H3C9, r% ' red component, 0 to 63 (brightness)
OUT &H3C9, g% ' green component, 0 to 63 (brightness)
OUT &H3C9, b% ' blue component, 0 to 63 (brightness)

Reading a colour
------------------
To read a colour information, ie, its RGB values, you have to OUT &H3C7 the 
colour you want, then INP from &H3C9, just like I said above to get the RGB 
values. Example:

OUT &H3C7, c% ' colour you want RGB for
r% = INP(&H3C9) ' get red component
g% = INP(&H3C9) ' get green component
b% = INP(&H3C9) ' get blue component

Note that they MUST be read in that order.

CODE !!!!!! here is what you want.

in order to save/load/change a colour, I suggest you use a TYPE that you can 
use to declare an array of colours, like the table the DAC keeps. You just 
have 0 to 255 colours and their RGB values. Just check the code. I've given 
you procedures to save and store the screen's palette... It would be alot 
better if you kept your own and made changes to it, if you want to I'll show 
you how, but these are the routines you wanted =)


'---------------------------------------------------------------------------
' PalSave -
'  desc: saves the 'visible' palette to palf$
' Example: PalSave "MYPAL.PAL"
'---------------------------------------------------------------------------
SUB PalSave (palf$)

DIM c%
DIM ff%
DIM byte AS STRING * 1

ff% = FREEFILE
OPEN (palf$) FOR OUTPUT AS #ff%: CLOSE #ff%
OPEN (palf$) FOR BINARY AS #ff%

FOR c% = 0 TO 255
 OUT &H3C7, c%
 byte = CHR$(INP(&H3C9)): PUT #ff%, , byte
 byte = CHR$(INP(&H3C9)): PUT #ff%, , byte
 byte = CHR$(INP(&H3C9)): PUT #ff%, , byte
NEXT c%

CLOSE #ff%

END SUB

'---------------------------------------------------------------------------
' PalLoad -
'  desc: loads the palette from a file and "puts" it onto the screen
'---------------------------------------------------------------------------
SUB PalLoad (palf$)

DIM c%
DIM ff%
DIM byte AS STRING * 1
DIM r%, g%, b%

ff% = FREEFILE
OPEN palf$ FOR BINARY AS #ff%
 FOR c% = 0 TO 255
  GET #ff%, , byte
  r% = ASC(byte)
  GET #ff%, , byte
  g% = ASC(byte)
  GET #ff%, , byte
  b% = ASC(byte)
  OUT &H3C8, c% ' colour you want to change, 0 to 255
  OUT &H3C9, r% ' red component, 0 to 63 (brightness)
  OUT &H3C9, g% ' green component, 0 to 63 (brightness)
  OUT &H3C9, b% ' blue component, 0 to 63 (brightness)
 NEXT c%
CLOSE #ff%

END SUB





