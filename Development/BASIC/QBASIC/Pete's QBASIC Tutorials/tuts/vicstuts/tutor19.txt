                     Vic's QBasic Programming Tutorial

                           Basic Tutorial XIX

                               DIRECT QB!!

Feeling left out of the QB scene because your games arn't as advanced because
    you use the original QB commands�  Try some New QB Commands!!!

-----------------------------------------------------------------------------

I am sure you have all seen it at some point...
DQB...
Maybe you saw it on the internet,
the program description says this...

"uses Direct QB"

You either say to yourself
1.) What the H* Is DQB?
2.) This game must look good...
3.) Ha ha!!... this loser can't even program in original QB!!
4.) Boy, I can't wait for Vic to cover this in a tutor!

well, maybe you don't even think of the last one...
But if you are wondering about this, or want to learn about this...
you have come to the right place...

---

What to do first...

1.) get the file...

I think that the homepage for DQB is...
http://echome.hypermart.net

not really sure if that is up,
but, if you can't find it there just go to my site...

http://members.aol.com/radiohands
and it will point you to a link to DQB...


If none of these work... Search the internet...

...

WHAT YOU WILL NEED TO USE AND COMPILE DQB...

what I can't stress enough, is this...

YOU NEED QB 4.5 OR HIGHER!!!

the old qbasic 1.1 doesn't cut it...

and once again I must point out that QB 4.5 is not for sale and is illegal to
download off the internet...  sorry...

but, I'm sure Microsoft won't sue you if you do happen to fall upon a link
to qb 4.5... (but I'm sure they would love to sue me for telling you this)

search for qb 4.5 on the internet or ask a friend...
Nuff' said...

------------------
ANOTHER IMPORTANT NOTE...

IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...
IF YOU HAVE EMS MEMORY TURNED OFF, YOU WILL NOT BE ABLE TO USE DQB...

there is a very very easy and quick solution to this problem...
and here it is...

If you know that EMS is turned off or you have no clue what I am talking
about do the following...

Step 1...

Open up NOTEPAD.EXE or EDIT.COM...
Goto FILE Then goto OPEN...

type this in the OPEN Field...

c:\config.sys

and hit enter
(note: the directory could be different on your computer...)

It should then open up the Config.sys file...
(might look something like this...)

DEVICE=C:\WINDOWS\HIMEM.SYS /TESTMEM:OFF
DEVICE=C:\WINDOWS\EMM386.EXE 4000
DOS=HIGH,UMB,AUTO
DEVICEHIGH=C:\WINDOWS\SYSTEM\CPQIDECD.SYS /D:IDECD001
FILESHIGH=80
BUFFERSHIGH=40,4
FCBSHIGH=10
SWITCHES=/F

What we are looking for is this...

DEVICE=C:\WINDOWS\HIMEM.SYS /TESTMEM:OFF
DEVICE=C:\WINDOWS\EMM386.EXE 4000

on some computers (newer win 95+ computers)
it might look like this...

DEVICE=C:\WINDOWS\HIMEM.SYS /TESTMEM:OFF
DEVICE=C:\WINDOWS\EMM386.EXE NOEMS

The NOEMS part is BAD!!!
This shuts off EMS memory...
and therefore DQB will not work...

So, if you have this, or dqb complains about EMS memory, do this...

DELETE the old

DEVICE=C:\WINDOWS\HIMEM.SYS /TESTMEM:OFF
DEVICE=C:\WINDOWS\EMM386.EXE NOEMS

or what have you... and type in this...

DEVICE=C:\WINDOWS\HIMEM.SYS
DEVICE=C:\WINDOWS\EMM386.EXE 4000 RAM

(the 4000 means 4 MB of memory,,, you can use more, but it is usually not
needed...)

then comes the annoying part...
you have to restart your computer

but, when your computer comes back online, DQB will work!!!!

-------------------


Ok, Youv'e downloaded the DQB ZIP file...
Youv'e set up EMS memory...
Now what?


---
COMPILING THE DQB LIBRARY FILES...

Ok...
please do the following...
(I assume you are running Win 95 or higher)
(if you arn't, and you are just using DOS then I am sure you are advanced
enough to change the directions into something usable...)



STEP 1...

Double click on MY COMPUTER (if in dos only type CD c:\)

goto the drive you want to use (usually c:\)
(from now on, I will use C:\ as the drive)(change if you need to)

now click FILE...
GOTO NEW and highlight and click FOLDER

It should make a new folder and allow you to type in the name of the folder
to be created...
Name the folder dqb

now, open the folder...
The folder should now Be EMPTY...

STEP 2...

Keep this folder in an open window and Use my computer to open the folder
containing QB 4.5...

To keep things clean, close every window except for
1.) This tutor... (duh)
2.) The DQB folder
3.) the QB 4.5 folder

Now, you have to make a copy these files from your qb 4.5 folder
do copy these files do RIGHT click (NOT LEFT!!!) and hold down the right
mouse button and drag them to the DQB folder...
when you let go of the RIGHT mouse button a menu should pop up,,,

On the pop-up menu left click on COPY HERE... (NOT CREATE SHORTCUT)
...

here are all the files that you NEED to compile the library and use the
library from your QB 4.5 directory...

Qb.exe
Bc.exe
Brun45.exe
Lib.exe
Link.exe
Bcom45.lib
Bqlb45.lib
Qb.lib
Qb.qlb

These are all the standard QB 4.5 files... (you may want to copy the QB help
file too...)

If you are missing any of these files you may not be able to compile the
library...

Step 3...

You may now Close the QB 4.5 directory (if you want)...

now, open up your copy of DQB (in winzip or whatever)
Now, you must extract the following files from the DQB zip file
(note, the dqb file on my site only has these files)
(so if you have the one from my site, extract all of them)

Extract the following files into the c:\dqb directory...
or, if you want, just extract the whole thing... (but it takes up Directory
space)...

Sound.obj
3d.obj
Disk.obj
Mouse.obj
Joystick.obj
Keyboard.obj
Palette.obj
Font.obj
Blending.obj
Bit.obj
Image.obj
Sprite.obj
Draw.obj
Main.obj
Dqbman.bas
Dqbman.mhl
Directqb.doc (important)
Errors.inc
Dqbtools.bas
Small.fnt
Eclogo.bas
Readme.1st
Filelist.txt
Script.fnt
Hitech.fnt
Eclogo.pcx

whew!!.....
(I recomend downloading the file from my page and just extracting them all!!)


Step 3...

Ok, the hard part is over...

Now,,,
Open up a DOS prompt...
(start / programs / MS-DOS Prompt)

in the dos prompt type this...

CD c:\dqb

and hit enter
this SHOULD take you to the DQB directory...

Now, type this...

qb /l

(yes that is a lower case L)
and hit enter...
This should open up QB with library support (needed for call absolute)

In QB goto File Then OPEN...
Open up the file called DQBMAN.BAS

And then run it...
if you get any errors, reread the above...

---------

This will open up the Direct QB library manager...
first, do what it says and press any key to continue...

A welcome sign should apear and say that this is the first time making this
library...

press any key...

it should show a list of library modules...

the first one (MAIN) should say that it is installed...

push the down button to highlight the next module...

Push the SPACE BAR (not enter) to activate this...

Now, if you wish ( i would recommend this) Activate all of the library
modules...

after all are activated hit the ENTER button...

A prompt should come up asking for the working QB directory
NOTE: do NOT use your old QB 4.5 directory!!!!!

In this prompt type this...

c:\dqb

and hit enter...

A "Summary of Operations" window will appear...
Just hit enter...

A new window will come up... and will show the library being created...

If this doesn't work all the way, you may be missing some of the needed files
and you MUST find them in order to use it...

----

If all this goes successfully, an overview of the commands used to make
the library will be displayed...

(((
If you find that the library wasn't created, look over the commands and
check for errors or missing files
)))

Now, hit enter and you will exit out of the program
now press any key to exit into QB...
(DO NOT CLOSE THIS WINDOW!!!)

GOT File and then EXIT to return to the DOS Prompt...

In the DOS prompt type this in...

qb /ldqb

This will open qbasic with the Direct QB library file...

GOT FILE and then OPEN

open the file called ECLOGO.BAS

and run it...

if dqb is configured correctly, you should see a neat display of the
Enhanced Creations Logo...

If this all goes correctly,

YOU NOW HAVE A WORKING VERSION OF THE DIRECT QB LIBRARY!!!!!!!!!

--------------

you may now exit out of the Dos Prompt...




------------------------
************************
------------------------

SETTING UP DQB AND QBASIC 4.5


---

one thing that I would recommend you do is this...

...

Open up another DOS Prompt...

type this in

CD c:\dqb

now type this

edit

this should open up the dos program EDIT.COM

In Edit, type this in at the first line...

qb /ldqb

Now, after you have typed that in, do this...

Click on FILE
then click on SAVE AS (not SAVE)
and type this in the save as box

runqb.bat

and hit enter...

you may now exit out of EDIT and the dos prompt...

Now goto your DQB directory from MY COMPUTER...

There should be a file called runqb.bat...

double click on it...

From now on, you will use this to open QBASIC...
When using the dqb library you can't just open QB.exe and expect anything
to happen but ERRORS!!!

From now on, you will use this to open QBASIC...
When using the dqb library you can't just open QB.exe and expect anything
to happen but ERRORS!!!

From now on, you will use this to open QBASIC...
When using the dqb library you can't just open QB.exe and expect anything
to happen but ERRORS!!!

got that�









------------------------
************************
------------------------

NOW FOR THE GOOD STUFF!!!!!!!!
Programming with DQB...


---

Ok, now we will actually get to be using the DQB library...

First, lets cover one thing we need...

At the beginning of any file that includes the DQB library you NEED this...

'$INCLUDE: 'DIRECTQB.BI'

or

REM $INCLUDE: 'DIRECTQB.BI'  (for all you know it alls)

(NOTE: if you don't have the DIRECTQB.BI file in your dqb directory
you are screwed... (go find it!!!))

What this does is set up all the directqb commands so you don't have to
do them individually in your program...

------

Now, lets start programming...






Goto your dqb directory and double click the file rundqb.bat...

this should open up a new qb session...

now, do you remember what to do first?
?
?

type in the '$INCLUDE: 'DIRECTQB.BI'
...

and now we are all set...
no, wait!!!
we have to initialize it...

after the '$INCLUDE: 'DIRECTQB.BI'
type this in...

x = DQBinit(1, 0)

This initializes Direct QB and gets it reved up for action...
(the 1,0 can be changed depending on your needs (see the dqb.doc))

Unless you want to work in just text mode, you may want to initialize the
screen...

with DQB there is only one type of screen... (but many screens for this)
it is SCREEN 13...
But, you can't start it by using the command SCREEN 13,,,
you have to use this...

DQBinitVGA


Now, lets talk a bit about the SCREEN mode DQB uses...
(this can seem somewhat advanced...)


----------------------

DIRECT QB SCREEN MODE... (yes mode, not modes)

This is very important that you somewhat understand how the Direct QB
screen works...

The dqb screen is just the plain old Screen 13...
BUT, you are able to use PAGES like screen 7 or screen 9...

How is that possible you ask?

It uses Virtual screens...

when you draw, you either draw to the "Actual" screen or the "Virtual" screen

in allmost every DRAW command (like dqbpset or dqbline)
it asks for a page to use...

If you want it to draw to a virtual (invisible) page you can
just put the page number...
example...

dqbpset 1,x,y,color

in order to see this pixel, you have to copy the Virtuall screen to
the actual screen...

you can do this by using this command...

DQBcopyLayer 1, VIDEO

this copies the virtual screen #1 to the Actual (VIDEO) screen...

So, if you really think about it... How could you draw a pixel directly to
the actual screen?

like this...

dqbpset VIDEO,x,y,color

...

do you understand...
I hope so...



---------------------
*********************
---------------------


Ok...
Now, you know everything there is to know about getting dqb set up...

LETS PROGRAM ALLREADY!!!!


If you look in the dqb.doc that came with DQB it will teach you everything
you need to know about each command that is included in DQB...
Study them!!
I will not cover them...


Our first DQB program!!!

Copy this code into your qb window...





'$INCLUDE: 'directqb.bi'
x = DQBinit(1, 0)

dqbinitvga
DQBinstallKeyboard

x = 320 / 2
y = 200 / 2
c = 1

DO
dqbpset 1, x, y, c
DQBcopyLayer 1, VIDEO
IF DQBkey(KEYLEFT) THEN x = x - 1
IF DQBkey(KEYRIGHT) THEN x = x + 1
IF DQBkey(KEYDOWN) THEN y = y + 1
IF DQBkey(KEYUP) THEN y = y - 1
dqbwait 1
LOOP UNTIL DQBkey(KEYESC)
dqbclose




Your first program with dqb!!!!!
and the last program for this tutor...

thank you...



----------------------------------------------------------------------------
****************************************************************************
----------------------------------------------------------------------------

   File  Edit  View  Search  Run  Debug  Calls  Options                   Help
+--------------------------------- Untitled -------------------------------�+-+
�Thats it for this tutorial, If I didn't get into enough detail in the         
�explanations then just look at the source code and try to figure it out
�on your own.  All else fails E-Mail Me...  I want to make these tutorials     _
�as easy to understand as posible!!!                                           _
�                                                                              _
�My current E-Mail address is RADIOHANDS@AOL.com                               _
�                                                                              _
�If you are using this tutorial on your page, please leave the tutorial        _
�exactly as it is... please don't change anything, unless its spelling         _
�errors... Theres alot of them! I don't like using the backspace key...        _
�                                                                              _
�The original website that these were on is                                    _
�                                                                              _
�http://members.aol.com/radiohands/index.html                                  _
�                                                                              _
�Thank you                                                                     _
�Vic Luce                                                                      _
�Finished                                                                      _
�July                                                                       _
�2000                                                                          _
�                                                                              
� ___________________________________________________________________________�
 <Shift+F1=Help> <F6=Window> <F2=Subs> <F5=Run> <F8=Step>     �     N 00001:008


...
If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)



