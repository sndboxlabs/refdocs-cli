


                     Vic's QBasic Programming Tutorial

                           Basic Tutorial VI

                              BIT BLT
			    (Bit Block)

How to place a sprite without writing over the background...

-----------------------------------------------------------------------------

If you have ever made a game using Get And Put You might notice how when
you put a sprite on the background it erases part of the Background in the
shape of a square. Heres an example, (Skip if you don't need an example...)

'----
DIM circ(1000)

SCREEN 13
CIRCLE (10, 10), 10
GET (0, 0)-(20, 20), circ
CLS
LINE (0, 0)-(320, 200), 7, BF
FOR i = 1 TO 500
LINE (RND * 320, RND * 200)-(RND * 320, RND * 200), (RND * 256)
NEXT
SLEEP 1
PUT (100, 100), circ, PSET

'--- stop copying!

How many times has this happend to you?
When I was just starting out using Get and put I couldn't stand it, I thought
that there would never be a solution to this problem... Guess what, There
is!  After searching many many Useless Qbasic websites I found absolutely
nothing on the subject.  I actually gave up Qbasic programming for a while, 
thinking that it was a totally useless and not worth the effort...  Then,
one day I went searching around the internet for nothing in particular and
came accross a programm that was obviously programmed by a genuis (That
means that it came with commented source code).  It was an RPG and it had
exactly what I was looking for.  Since then there has been several programs
that use this tequnique, but not when I tried to learn it...

This is something that you would most likely not come accross on your own,
What you actually to Is place a mask on the screen and the put your image
down.  If you are just learning this you most likely have no Idea what the
heck I'm Talking about.  This requires Two images of the same object...

1. The Mask
2. The actual Image

---

What a mask is...

A mask Is an exact copy of the actual image only that everything that
is supposed to be the background is put as the color 255...
Think of the color 0 (Black) as The Images Background and color 255 as the
Masks background...
Look at it like this... Remember what a bitmap is??

This is the image

004,000,000,000,004
000,004,000,004,000
000,000,004,000,000      Bitmap = 5 x 5
000,004,000,004,000
004,000,000,000,004

If you PSET this to the screen it would look like a red (004 = 4) X...
Heres what Its Mask would look like...

004,255,255,255,004
255,004,255,004,255
255,255,004,255,255      Bitmap = 5 x 5
255,004,255,004,255
004,255,255,255,004

Notice: Everything that was Black (the number 000) is now the color 255...
        Everything that was Black is considered the background, same as
        the color 255 (We will call this the invisible color...)

Great, Now what do we do with this?

First, We must use the command Get to get both images.  It would be a good
Idea to save one as Image and the other as ImageMask Or ImageSH (SH is for
Shadow)...

Once we have both images in an array or whatever, you have to put them on the
screen in a special way.  Do it like this...

PUT (X,Y), ImageMask, AND
PUT (X,Y), Image, OR

OR you could do it like this

PUT (X,Y), ImageMask, AND
PUT (X,Y), Image, XOR

(Big difference huh?)

That is how you do it, Now it is time for an example!

'---- Start Copying
'First Dim The sprites that will be made 
'It could be less than 1000, but why not...
DIM circsh(1000), circ(1000)

SCREEN 13
'This draws the IMAGE

CIRCLE (10, 10), 10, 4
PAINT (10, 10), 4, 4
GET (0, 0)-(20, 20), circ

'This bottom command draws the entire background as the
'Color 255 (Invisible Color) so the sprite can be drawn
'Over IT.  This Draws the Mask...

LINE (0, 0)-(20, 20), 255, BF  

CIRCLE (10, 10), 10, 4
PAINT (10, 10), 4, 4
GET (0, 0)-(20, 20), circsh

'The rest of the crap below just draws the screen...
CLS
LINE (0, 0)-(320, 200), 7, BF
FOR i = 1 TO 500
LINE (RND * 320, RND * 200)-(RND * 320, RND * 200), (RND * 256)
NEXT
SLEEP 1  'This just stops the program for a second...

'This is where the Mask Is put Down and then The actual
'Image is put down over it.
PUT (100, 100), circsh, AND
PUT (100, 100), circ, OR

'--- STOP!!!


Amazing huh, 
Well, Maybe not, but If you have been going nuts trying to figure it out
then this could be a life saver...

--------------------------------------------------------------------------

If you don't understand it at first, Just look through it over and over, If
you still don't get it after that, just copy the code and pretend you know
how it works, I won't tell...

If you only want to program in something that has 256 colors then this works
fine...  But if you want to program in a screen mode that only has 16
colors then you have to do something different.  Why would you want to
program in another screen mode you may ask...  In screen 7 you have many
pages to work with,  I havn't explained pages yet, but if you know what
they are then you know how great they are to programming...

Here is what you do in screen mode 7...

Instead of having the color 0 = 255 have 0 = 15 (white)
Then, Instead of having the sprite in the mask the same color use the color
0.  Heres what it would look like in bitmap form...

This is the image

04,00,00,00,04
00,04,00,04,00
00,00,04,00,00      Bitmap = 5 x 5
00,04,00,04,00
04,00,00,00,04

If you PSET this to the screen it would look like a red (04 = 4) X...
Heres what Its Mask would look like...

00,15,15,15,00
15,00,15,00,15
15,15,00,15,15      Bitmap = 5 x 5
15,00,15,00,15
00,15,15,15,00

Look at that, None of the colors go over 15 (maximum color)!

To prove that this will work here is the same example, only in screen
7 (16 colors (0-15))

'----- Begin

DIM circsh(1000), circ(1000)

SCREEN 7
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 4, 4
GET (0, 0)-(20, 20), circ
LINE (0, 0)-(20, 20), 15, BF
CIRCLE (10, 10), 10, 0
PAINT (10, 10), 0, 0
GET (0, 0)-(20, 20), circsh

CLS
LINE (0, 0)-(320, 200), 7, BF
FOR i = 1 TO 500
LINE (RND * 320, RND * 200)-(RND * 320, RND * 200), (RND * 16)
NEXT
SLEEP 1
PUT (100, 100), circsh, AND
PUT (100, 100), circ, OR

'---- END

You could do this the exact same way in screen 13, but that seems to be
a more complicated way to do it...
-----------------------------------------------------------------------------

This is not the only way to get the sprite on the screen without covering
the background.  One way that I use a lot is to use a bitmap.
With this technique you only PSET Pixels that are not the color that you
want to use for the background, (0)...

Maybe I'll Talk about this in a later tutorial...

I hope this helps any of you who have had this question...
-----------------------------------------------------------------------------
Thats it for this tutorial, If I didn't get into enough detail in the
explanations then just look at the source code and try to figure it out
on your own.  All else fails E-Mail Me...

My current E-Mail address is RADIOHANDS@AOL.com

If you are using this tutorial on your page, please leave the tutorial 
exactly as it is... please don't change anything, unless its spelling
errors... Theres alot of them! I don't like using the backspace key...

The original website that these were on is

http://members.aol.com/radiohands/index.html

Thank you
Vic Luce
Finished
November 8
1999

If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)
