     The QBNews                                                     Page 17
     Volume  1, Number  5                                 December  1, 1990

     Self-Cloning Exe's Revisted by Ronny Ong
     
          In  Volume 1, Number 3 of QBNews, Larry Stone presented  "How  to
     Make  a  Self-Cloning  Exe in QuickBASIC."  In his  sample  code,  you
     needed  to  define a value for MyProg$, the filename of the  EXE  that
     would  be accessing itself.  Larry did not go into the  considerations
     involved in defining MyProg$.  This article will.
     
          You could simply hard-code a filename into the program, but  that
     would  prevent users from renaming it, an inconvenience.  The  program
     could  ask  users  for  its own  filename,  making  the  program  look
     unnecessarily  dumb.  You would also have the headache of getting  and
     validating  input, and handling all the possible errors  like  invalid
     filenames  and  valid filenames which are simply wrong, i.e.  not  the
     true name of the program.
     
          And  then there is the drive/path specification.   You  certainly
     cannot hard-code that.  You might get away with forcing users to  keep
     the program in a certain directory, but not on a certain drive.  There
     are  too many different hardware configurations in today's  PC  world.
     Think you can leave off the drive/path and let DOS look in the default
     drive/path?  Think again:
     
     (1)   What  if  the program is located in the DOS PATH?   It  can  get
     loaded  even  if  it is not in the default drive/path,  but  the  OPEN
     statement  will not find it (unless users just happen to use DPATH  or
     APPEND and just happen to specify the same drive/path as in PATH).
     
     (2)   What if the program gets executed through PC Magazine's  RUN.COM
     utility, which searches all directories on a drive regardless of PATH?
     
     (3)  What if users execute the program from a different drive/path  by
     specifying the drive/path of the program along with the filename?
     
     Fortunately, there is a way to avoid all these problems.
     
          The  sample  program contained in FILESPEC.BAS  demonstrates  how
     programs  written in QuickBASIC Version 4 or higher and  compiled  and
     linked to an EXE file can find out the complete drive, path, filename,
     and  .EXE  extension the program was loaded as.  This  information  is
     stored in memory when DOS loads a program through its "EXEC" function.
     The  underlying  approach can be applied to COM or EXE  files  in  any
     language.
     
          When   incorporating  this  technique  into   your   self-cloning
     programs,  you  should  get  to  the  OPEN  statement  immediately  if
     possible, in case users try to execute your program from diskette  and
     then  remove the diskette.  If you can put this code and your  I/O  to
     the EXE up front, the disk activity will look like it is part of DOS's
     loading of your program.
     
          Hard-coding  may look like an easy way out when compared  to  the
     code  above,  but if your program is good enough to  deserve  a  self-
     cloning feature, then it deserves the flexibility of this approach.
     
     The QBNews                                                     Page 18
     Volume  1, Number  5                                 December  1, 1990

     
     **********************************************************************
     Ronny Ong is a mainframe systems engineer.  He can be reached at
     P.O. Box 260796, Plano, TX  75026-0796.
     **********************************************************************
     
     [EDITOR'S NOTE]
          All  code  and files for this article can be found  in  the  file
     FILESPEC.ZIP.