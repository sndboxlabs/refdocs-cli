     ----------------------------------------------------------------------
        W h o   y a   g o n n a   c a l l ?   C A L L   I N T E R R U P T
     ----------------------------------------------------------------------

     Format Floppy Disks with QB  by Cornel Huth
     
          You just wrote a great new program that does everything you could
     possibly want it to do but one thing - format a blank disk.  You could
     have  QB  shell  to  DOS and run FORMAT,  but  what  if  FORMAT  isn't
     available?   Even  if it is, maybe you'd rather not shell  because  it
     looks  sloppy.  Other programs can format blank disks so you ought  to
     be able to do it, too.  Right?  Well, now you can.  QBFORMAT.BAS is  a
     plug-and-play  QB4 module that will format a DOS disk with one  simple
     function call.
     
          You wil need QB4 or higher and a floppy disk.  The code  supports
     360K drives for XTs and 360K and 1.2M drives for ATs.  720K and  1.44M
     3.5- inch drives should format just fine on ATs but without the actual
     hardware  I  can't  verify that it does.  I also  can't  find  XT-BIOS
     support  documentation for the 3.5-in drives so you'll have to do  the
     digging and add the code if you need to support those drives on an XT.
     
          To use the code, call the routine with the drive and media  type.
     Floppy  drives start at 0, i.e., drive A: is 0, B: is 1 (no,  your  C:
     hard  drive is not 2, it's 128, so don't worry).  The media types  are
     described  in the source by CONST declarations.  By doing the same  in
     the  calling program the call to format a disk would be as  simple  as
     this:
     
          xerr%  =  QBFORMAT%(0, DS9)
          IF xerr% THEN
               errl% = xerr% \ 256
               errc% = xerr% AND 255
          END IF
     
          0  is  drive A:, DS9 is the media byte for a  360K  floppy.   The
     function returns a multiplexed error code.  The high byte is the level
     at  which the error occurred and the low byte is the BIOS error  code.
     See the source for more along with descriptions of the level and error
     codes.
     
          Somebody goofed when he assigned the media byte for 720K 3.5-inch
     floppies.   He  used  the same media byte as  the  1.2M  5.25-inch  AT
     floppy.   To differentiate the two, QBFORMAT expects 1.2M  media  byte
     arguments  to  be positive &HF9 and 720K media byte  arguments  to  be
     negative &HF9 (-&HF9).  See the source for more.
     
          The  source  is all QB except for the INTERRUPTX  routine.   This
     routine is supplied by Microsoft in the QB.LIB/QLB file and the source
     is  INTRPT.ASM.  QB 4.00's INTRPT.ASM has 2 bugs, one so bad  that  it
     can  not be used with QBFORMAT.BAS.  QB 4.00b has one bug, not so  bad
     that it can't be used with QBFORMAT but it does prevent the error code
     from being returned properly if you call INT 25/26h (QBFORMAT calls on
     BIOS  INT  13h).   Included  with the  QBFORMAT  code  should  be  the
     
     The QBNews                                                     Page 12
     Volume  1, Number  5                                 December  1, 1990

     corrected version of INTRPT.OBJ.  Hopefully the ASM source can also be
     included  (if space allows).  Replace your INTRPT.OBJ with  this  one,
     especially if you are using QB 4.00 (initial release).
     
          The  code  speaks for itself (it does to me).  If  you're  having
     trouble  understanding the code, drag out the old QB manual and  piece
     through the code.  If the logic behind MS-DOS formatting is  difficult
     to  follow, consult a PC BIOS book such as those by Peter Norton,  Ray
     Duncan  (his Advanced MS-DOS), the Phoenix people or  Robert  Jourdain
     (one alone probably won't do it).  PCDOS texts usually won't cover the
     BIOS  to  the required detail so those won't be of much  help  in  the
     format  process.   However, they may provide you with details  of  the
     structure  of  the  boot  record, file  allocation  tables,  and  root
     directory.
     
     **********************************************************************
          Cornel  Huth  is the author of various shareware  and  commercial
     tools  for  use  with  QuickBASIC. He  can  be  reached  through  this
     newsletter.
     **********************************************************************
     
     [EDITOR'S NOTE]
     
          All  code  and files for this article can be found  in  the  file
     QBFORMAT.ZIP. I have tested this code and found it to work on all disk
     types on MY 386. You should thoroughly test this code on any  machines
     you  plan to use. If you find any problems, I am sure Cornel would  be
     interested in hearing from you.