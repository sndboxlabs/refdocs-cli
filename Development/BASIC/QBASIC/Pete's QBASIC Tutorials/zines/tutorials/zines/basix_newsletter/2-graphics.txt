=============
 2. Graphics
=============

Last month we wrote a SUB that would plot pixel directly to the graphics memory, and I asked if 
anyone could send in the same SUB, but in inline assembler so here it is courtesy of Bucko.

>Quick ASM Proggy
>
>MOV ES, A000h  ;Extra Segment = Video buffer
>MOV BX, X      ;BX = X
>MOV AX, Y      ;AX = Y
>MOV CL, 8
>SHL AX, CL     ;AX = Y * 256
>ADD BX, AX     ;BX = X + Y * 256
>MOV AX, Y      ;AX = Y
>MOV CL, 6
>SHL AX, CL     ;AX = Y * 64
>ADD BX         ;AX = BX + Y * 64 = X + Y * 320
>MOV DI, AX     ;Offset = Offset of pixel
>MOV AL, Colour ;AL = Colour
>MOV [ES:DI],CL ;Set The pixel
>
>Probably could be shorter and/or faster, but it works (I hope - it's untested ~^.^~ and it 
>doesn't store the old stuff)
>
>Here's the Blast! library version of the same...
>
>PUSH    DS              ;Save the Destination Segment
>PUSH    BP              ;Save the Base Pointer
>
>MOV     BP,SP           ;Get the Stack Pointer
>MOV     AX,[BP+10]      ;Get the to buffer segment
>MOV     DS,AX           ;and set DS to it.
>MOV     SI,[BP+0A]      ;Get the Y position.
>
>MOV CL,06 ;Multiply it by 64 by using a Shift
>SHL SI,CL ;Left (SHL) for speed.
>MOV BX,SI ;Save the result temporarily.
>MOV CL,02 ;Shift left again to multiply the
>SHL SI,CL ;Y position by 256, then add that
>ADD SI,BX ;value to our saved result.
>MOV BX,[BP+0C] ;Now get the X position and add it
>ADD SI,BX ;to the result to get our final
>MOV BX,[BP+0E] ;offset. Then get the To buffer
>ADD SI,BX ;offset and add the pixel offset.
>MOV AL,[BP+08] ;Get the pixel color,
>MOV [SI],AL ;and plot it.
>
>POP     BP              ;Reset the Base Pointer
>POP     DS              ;Reset the Destination Segment
>RETF    000A            ;Return to BASIC Program, clean up
>
>It's in sub for but hey! What isn't... (it was built for a CALL ABSOLUTE)

It's been edited becuase of some errors, but it should be all right now.

RGB Palette

	As you may or may not know, there are only three primary colours for light: RED, GREEN 
and BLUE (not to be confused with paints: RED BLUE and YELLOW) to make pixtures on your screen 
the monitor displays each pixel as different amounts of RED GREEN and BLUE, this allows it to 
display a large range of colours. 
	But there is a problem: the graphics memory, there is only a certain amount of graphics 
memory so even though in SCREEN 13 there are 64^3 possible colours only 256 can be displayed at 
one time, but we can change how much RED GREEN and BLUE a certain colour has by OUTPUTTING to the
graphics card, this allows for very good special effects such as screen fading on some games.
	To change the PALETTE we have to do four commands, we have to output the number of the 
colour to port &H3C8 and then output the RED, GREEN and BLUE to port &H3C9, to output the RGB 
value we do them one at a time in the order RED, GREEN and BLUE.
	Note you can only use values 0-63 when you are changing the RGB values.

SCREEN 13
DELAY = 100  'Increase or decrease this depending on your machine speed
             'This will make the screen flash from red to black
DO
FOR C% = 0 TO 63
SETPAL 0, C%, 0, 0 
FOR D = 0 TO DELAY: NEXT D
NEXT C%
FOR C% = 63 TO 0 STEP -1
SETPAL 0, C%, 0, 0
FOR D = 0 TO DELAY: NEXT D  
NEXT C%
LOOP WHILE INKEY$ = ""


SUB SETPAL (COL%, R%, G%, B%)
  OUT &H3C8, COL%   'Output the colour
  OUT &H3C9, R%     'Red value
  OUT &H3C9, G%     'Green value
  OUT &H3C9, B%     'Blue value
END SUB

	Does anyone know how to do that in Inline assembler? Here is how that works, SETPAL(our 
PALETTE changing function) is called and changes the background colour 0, to pure red (0 to 63 
are the different shades of red and this is what causes the fade effect) and then back to black.

	If you have a paint program and you want to use a colour from it, the chances are that 
the program will give you numbers from 0-255 not 0-63, to convert them so that you can use them 
divide all the RGB values by 4.

some sample RGB values
         
BLACK  - 0,0,0
WHITE  - 63,63,63
YELLOW - 63,63,0
RED    - 63,0,0
BLUE   - 0,0,63
GREEN  - 0,63,0
PURPLE - 63,0,63

Next months issue, reading the PALETTE and reading graphics memory.


----------------------------------------------------------------------------------
This tutorial originally appeared in the BASIX Newsletter, Issue 2 from July, 1999.
It was written by Peter Johnson (a.k.a. Screech)