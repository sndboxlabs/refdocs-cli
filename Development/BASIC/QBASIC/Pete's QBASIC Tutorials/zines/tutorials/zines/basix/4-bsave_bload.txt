______________________________________________________________________________
| SECTION 1 PART B SUBPART 2 | Graphics tips and tactics |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 This article is by James Erickson, he can be reached at:
	ericksnj@teleport.com
 For how long I don't know.. he changes email address a lot. 8-)
 Cheers, James 8-)

Have you ever wanted to save a graphic in BASICs Screen 13.  Or more 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
importantly load one quickly?  Well its easy!  Here's how...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
First you'll need to draw something on the screen.. It doesn't matter
how.   Then you need to save it.  The first step is to point to the 
segment of the video address.  Then you want to bSAVE the graphic:

DEF SEG=&HA000
BSAVE "filename",0,64000

64000 will save the entire screen.  So use that to save an entire screen.
It works sort of like Peter Coopers putpixel.  Just multiply the y value
by 320 and the x value and add by one.  So if you only wanted to save the 
upper half of the screen it would be ((y*320)+x)+1.  In this case y would 
be 99 and x would be 319.  The reason you need to add by one is when you 
are saving the graphic it thinks of (0,0) as 1 instead of 0.  So that means
you have to add the entire thing by one.  So once again here is the equation:
((Y*320)+X)+1

The parenthesis are not really needed, but they are good to put in.

To Load the screen back up later, just do this simple thing:
Def Seg=&HA000
BLOAD "filename"

That's it!!!
-----------------------------------------------------------------------------

Well those loading and saving routines are all well and good if you want 
to grab the entire screen, or the upper parts, but what if you want to save
and load a sprite?  Well that is a little more complex, but it is still 
easy!

First as usual you draw something on the screen.  Then you need to make 
an array for your sprite.  I am not sure how you demention your array 
based on the graphic size, because the demensions of the array can be 
smaller then the sprite's demensions!  So if anyone knows what the equation 
is for dimensioning an array based on the sprites dimensions then I would 
LOVE to know.  But until then I just dimention the array according to the 
sprite size.  So if the sprite is 20x20 the the I make the array 20x20.  
What I do is say

Dim array(20*20) or..
Dim array(20,20)

Then you need to get the image.  If you don't know how GET works then look 
it up in you BASICs help file.

Like..
GET (1,1)-(20,20),array

Now you need to point to that array like so..

DEF SEG=VARSEG(array(0))

Now it is safe to save it.  Like so...

BSAVE "filename",Varptr(array(0)),length

The equation for the length of the file does not work for saving a sprite 
either.  it is not X*Y, so I don't know what the equation for it is.  So 
I hope someone who knows the equation can contribute it to the fanzine.
Meanwhile for length I usually use 16384, but if the sprite does not load 
up all the way when you try to load it, then increase that number.
VARPTR tells BSAVE where to look for the info.  In this case it is the
memory location where the array is stored.

OK, now that you have the image saved, you can use it in one of your
programs.  But this time when you load up the graphic, you load it into
an array instead of to the screen.  Don't worry it is just as fast.

First dimension your array, the way you did before...

Dim array(20*20) or..
Dim array(20,20)

Now point to that array again:

DEF SEG=VARSEG(array(0))

Now Load the graphic the same way you did before, but this time load it into
the array.  Like so:

BLOAD "filename",VARPTR(array(0))

Now that it is in the array you can put it on the screen whenever you want!
Just use PUT.  If you don't know what PUT is then look it up in the online
help.  But here is the general idea...

PUT (x,y),array,PSET

I Hope I have not further confused you, but nevertheless those things work.
----------------------------------------------------------------------------

Ok, now you can load and save graphics, but what about palettes?  
Well palettes are easy too.  But I think I'll save that for next Fanzine.
I'll probably make a PCX to BSAVE format, some more advanced graphics 
arrays, and some fade in and out subs as well.

 I'm sure people'll be looking foward to seeing the PCX to BSAVE thing.
 Palettes  too!  8-)  Thank you James. This sort of tutorial is exactly
 what  the  fanzine needs. Can anyone else write tutorials? I'd love to
 publish them... for example do you control your house with a QBasic 
 program or little robots etc... probably not but if you do I would love
 to hear from you.






--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #4 from January 1996.