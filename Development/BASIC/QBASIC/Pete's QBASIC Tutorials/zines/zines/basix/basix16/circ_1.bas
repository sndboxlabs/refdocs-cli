'==============Don's Subroutine that worked for circling=================
'
'From:  Don Schullian
SCREEN 9: CLS

MyColors:
  DATA 01, 320
  DATA 01, 320
  DATA 09, 310
  DATA 02, 310
  DATA 02, 310
  DATA 14, 310
  DATA  0

RESTORE MyColors
x% = 320
Y% = 300

DO
  READ Colour%
  IF Colour% = 0 THEN END
  READ x%

  FOR Radius% = 1 TO 300 STEP 3
    CIRCLE (x%, Y%), Radius%, Colour%
  NEXT Radius%

  Y% = Y% + 10

LOOP
