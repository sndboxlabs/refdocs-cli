This makes use of HTML5. The <img src> is for backwards compatibility and as a default since the following example uses different images depending on the screen size, which may be useful for things such as mobile phones.

<picture>
  <source media="(min-width: 650px)" srcset="./AltMainImage_650.png">
  <source media="(min-width: 465px)" srcset="./AltMainImage_465.png">
  <img src="./MainImage.png" alt="StreamPi Icon" style="width:auto;">
</picture>
