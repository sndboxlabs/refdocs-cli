usage: presentty [-h] [--light] [--warnings] file

Console-based presentation system

positional arguments:
  file        presentation file (RST)

optional arguments:
  -h, --help  show this help message and exit
  --light     use a black on white palette
  --warnings  print RST parser warnings and exit if any
