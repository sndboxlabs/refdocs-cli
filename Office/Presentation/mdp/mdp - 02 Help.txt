Usage: mdp [OPTION]... [FILE]
A command-line based markdown presentation tool.

  -d, --debug       enable debug messages on STDERR
                    add it multiple times to increases debug level
  -e, --expand      enable character entity expansion
  -f, --nofade      disable color fading in 256 color mode
  -h, --help        display this help and exit
  -i, --invert      swap black and white color
  -t, --notrans     disable transparency in transparent terminal
  -s, --noslidenum  do not show slide number at the bottom
  -v, --version     display the version number and license
  -x, --noslidemax  show slide number, but not total number of slides

With no FILE, or when FILE is -, read standard input.
