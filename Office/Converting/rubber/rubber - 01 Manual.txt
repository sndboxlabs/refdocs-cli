RUBBER(1)                            General Commands Manual                            RUBBER(1)

NAME
       rubber - a building system for LaTeX documents

SYNOPSIS
       rubber [options] sources ...
       rubber-pipe [options]

DESCRIPTION
       Rubber  is  a  wrapper  for  LaTeX  and companion programs.  Its purpose is, given a LaTeX
       source to process, to compile it enough times to resolve all references, possibly  running
       satellite  programs  such as BibTeX, makeindex, Metapost, etc. to produce appropriate data
       files.

       The command rubber builds the specified documents completely.  The  source  files  may  be
       either LaTeX sources (in which case the suffix .tex may be omitted) or documents in a for‐
       mat Rubber knows how to translate into LaTeX.  If one compilation fails, the whole process
       stops,  including  the  compilation  of the next documents on the command line, and rubber
       returns a non-zero exit code.

       The command rubber-pipe does the same for one document but it reads the LaTeX source  from
       standard input and dumps the compiled document on standard output.

       Some  information  cannot  be  extracted  from  the  LaTeX sources.  This is the case, for
       instance, with the search paths (which can be specified in environment variables like TEX‐
       INPUTS),  or  the  style  to be used with Makeindex.  To address this problem, one can add
       information for Rubber in the comments of the LaTeX sources, see section DIRECTIVES.

OPTIONS
       The options are used either to choose the action to  be  performed  or  to  configure  the
       building process.  They are mostly the same in rubber and rubber-pipe.  Options are parsed
       using GNU Getopt conventions.

       -b, --bzip2
              Compress the final document (in bzip2 format).  This is  equivalent  to  saying  -o
              bzip2 after all other options.

       --clean
              Remove  all  files  produced  by the compilation, instead of building the document.
              This option is present in rubber only.  It applies to the compilation as  it  would
              be  done  with  the  other options of the command line, i.e. saying "rubber --clean
              foo" will not delete foo.ps, while saying "rubber --ps --clean foo" will.

       -c, --command <command>
              Execute the specified command (or directive) before parsing the input  files.   See
              section DIRECTIVES for details.

       -e, --epilogue <command>
              Execute  the  specified  command (or directive) after parsing the input files.  See
              section DIRECTIVES for details.

       -f, --force
              Force at least one compilation of the source.  This may be useful, for instance, if
              some unusual dependency was modified (e.g.  a package in a system directory).  This
              option is irrelevant in rubber-pipe.

       -z, --gzip
              Compress the final document (in gzip format).  This is equivalent to saying  -o  gz
              after all other options.

       -h, --help
              Display the list of all available options and exit nicely.

       --inplace
              Go  to  the  directory  of  the  source files before compiling, so that compilation
              results are in the same place as their sources.

       --into <directory>
              Go to the specified directory before compiling, so  that  all  files  are  produced
              there and not in the current directory.

       --jobname <name>
              Specify  a  job  name  different from the base file name.  This changes the name of
              output files and only applies to the first target.

       -k, --keep
              This option is used in rubber-pipe only.  With this  option,  the  temporary  files
              will  not  be removed after compiling the document and dumping the results on stan‐
              dard output.  The temporary document is named rubtmpX.tex, where X is a number such
              that no file of that name exists initially.

       -l, --landscape
              Specify that the final document should use landscape orientation.  This is relevant
              only when using dvips or dvipdfm.

       -n, --maxerr <num>
              Set the maximum number of displayed errors.   By  default,  up  to  10  errors  are
              reported, saying -n -1 displays all errors.

       -m, --module <module>[:<args>]
              Use  the specified module in addition to the document's packages.  Arguments can be
              passed to the package by adding them after a colon, they correspond to the  package
              options in LaTeX.  The module is loaded before parsing the document's sources.

       --only <sources>
              Compile  the  document partially, including only the specified sources.  This works
              by inserting a call to \includeonly on the command line.  The argument is a  comma-
              separated list of file names.

       -o, --post <module>[:<args>]
              Use  the  specified  module as a post-processor.  This is similar to the -m options
              except that the module is loaded after parsing the document.

       -d, --pdf
              Produce PDF output.  When this option comes after --ps (for instance  in  the  form
              -pd) it is a synonym for -o ps2pdf, otherwise it acts as -m pdftex, in order to use
              pdfLaTeX instead of LaTeX.

       -p, --ps
              Process the DVI produced by the process through dvips(1) to  produce  a  PostScript
              document.  This option is a synonym for -o dvips, it cannot come after --pdf.

       -q, --quiet
              Decrease the verbosity level.  This is the reverse of -v.

       -r, --read <file>
              Read additional directives from the specified file (see also the directive "read").

       -S, --src-specials
              Enable  generation of source specials if the compiler supports it.  This is equiva‐
              lent to setting the variable src-specials to yes.

       -s, --short
              Display LaTeX's error messages in a compact form (one error per line).

       -I, --texpath <directory>
              Add the specified directory to TeX's search path.

       --synctex
              Enable SyncTeX support in the LaTeX run.

       --unsafe
              Permit the document to invoke arbitrary external  programs.   This  is  potentially
              dangerous, only use this option for documents coming from a trusted source.

       -v, --verbose
              Increase the verbosity level.  Levels between 0 and 4 exist, the default level is 1
              for rubber and 0 for rubber-pipe.  Beware, saying -vvv makes Rubber speak a lot.

       --version
              Print the version number and exit nicely.

       -W, --warn <type>
              Report information of the given type if there was no error during compilation.  The
              available  types are: boxes (overfull and underfull boxes), refs (undefined or mul‐
              tiply defined references), misc (other warnings) and all to report all warnings.

MODULES
       Rubber's action is influenced by modules.  Modules take care of the particular features of
       packages and external programs.

   Packages
       For every package that a document uses, Rubber looks for a module of the same name to per‐
       form the tasks that this package my require apart from the compilation by LaTeX.   Modules
       can be added to the ones provided by default to include new features (this is the point of
       the module system).  The standard modules are the following:

       asymptote
              Process the .asy files generated by the LaTeX package, then triggers  a  recompila‐
              tion.

       beamer This module handles Beamer's extra files the same way as other tables of contents.

       bibtex, biblatex
              Takes care of processing the document's bibliography with BibTeX when needed.  This
              module is automatically loaded if the document  contains  the  macro  \bibliography
              (see also in DIRECTIVES for options).

       combine
              The  combine  package  is used to gather several LaTeX documents into a single one,
              and this module handles the dependencies in this case.

       epsfig This modules handles graphics inclusion for the documents that use  the  old  style
              \psfig  macro.   It  is actually an interface for the graphics module, see this one
              for details.

       glossaries
              Run makeglossaries and recompiles when the .glo file changes.

       graphics, graphicx
              These modules identify the graphics included in the document and consider  them  as
              dependencies  for  compilation.   They also use standard rules to build these files
              with external programs.  See the info documentation for details.

       hyperref
              Handle the extra files that this package produces in some cases.

       index, makeidx, nomencl
              Process the document's indexes and nomenclatures with makeindex(1) when needed (see
              section DIRECTIVES for options).

       ltxtable
              Add dependencies for files inserted via the ltxtable LaTeX package.

       minitoc, minitoc-hyper
              On  cleaning,  remove additional files that produced to make partial tables of con‐
              tents.

       moreverb, verbatim
              Adds the files included with \verbatiminput and  similar  macros  to  the  list  of
              dependencies.

       multibib
              Handles  the  extra bibliographies that this package creates, and removes the extra
              files on cleaning.

       xr     Add additional .aux files used for external references to the list of dependencies,
              so recompiling is automatic when referenced document are changed.

   Pre-processing
       The  following modules are provided for using programs that generate a LaTeX source from a
       different file format:

       cweb   This module's purpose is to run cweave(1) if needed before the compiling process to
              produce  the  LaTeX source.  This module is automatically loaded if the file speci‐
              fied on the command line has .w as its suffix.

       lhs2TeX
              This module uses the lhs2TeX preprocessor to generate the LaTeX source from a  Lit‐
              erate Haskell program.  It is automatically triggered if the input file's name ends
              with .lhs.

   Post-processing
       The following modules are provided to support different kinds of  post-processings.   Note
       that  the  order  matters  when using these modules: if you want to use a processing chain
       like
              foo.tex -> foo.dvi -> foo.ps -> foo.pdf -> foo.pdf.gz
       you have to load the modules dvips, ps2pdf and gz in that order, for  instance  using  the
       command line
              rubber -p -o ps2pdf -z foo.tex

       bzip2  Produce a version of the final file compressed with bzip2(1).

       dvipdfm
              Runs dvipdfm(1) at the end of compilation to produce a PDF document.

       dvips  Runs  dvips(1)  at  the  end of compilation to produce a PostScript document.  This
              module is also loaded by the command line option --ps.

       expand Produce an expanded LaTeX source by replacing \input macros by included files, bib‐
              liography  macros  by the bibliography produced by bibtex(1), and local classes and
              packages by their source.  If the main file is foo.tex then then expanded file will
              be named foo-final.tex.  See the info documentation for details.

       gz     Produce a version of the final file compressed with gzip(1).

       ps2pdf Assuming  that  the  compilation produces a PostScript document (for instance using
              module dvips), convert this document to PDF using ps2pdf(1).

   Compiler choice
       The following modules are used to change the LaTeX compiler:

       aleph  Use the Aleph compiler instead of TeX, i.e. compiles the  document  using  lamed(1)
              instead of latex.

       omega  Use  the  Omega compiler instead of TeX, i.e. compiles the document using lambda(1)
              instead of latex.  If the module dvips is used too, it will use odvips(1) to trans‐
              late the DVI file.  Note that this module is triggered automatically when the docu‐
              ment uses the package omega.

       pdftex Instructs Rubber to use pdflatex(1) instead of latex(1) to compile the document. By
              default,  this  produces  a  PDF file instead of a DVI, but when loading the module
              with the option dvi (for instance by saying -m pdftex:dvi) the document is compiled
              into  DVI  using  pdflatex.   This module is also loaded by the command line option
              --pdf.

       vtex   Instructs Rubber to use the VTeX compiler.  By default this uses vlatex as the com‐
              piler  to  produce  PDF  output.   With  the option ps (e.g. when saying "rubber -m
              vtex:ps foo.tex") the compiler used is vlatexp and the result is a PostScript file.

       xelatex
              Instructs Rubber to use xelatex(1) instead of latex.

DIRECTIVES
       The automatic behavior of Rubber is based on searching for macros in  the  LaTeX  sources.
       When this is not enough, directives can be added in the comments of the sources.  A direc‐
       tive is a line like
              % rubber: cmd args
       The line must begin with a "%", then any sequence of "%" signs and spaces, then  the  text
       "rubber:"  followed  by  spaces  and a command name, possibly followed by spaces and argu‐
       ments.

   General directives
       alias <name1> <name2>
              Pretend that the LaTeX macro name1 is equivalent to name2.  This can be useful when
              defining wrappers around supported macros.

       clean <file>
              Indicates that the specified file should be removed when cleaning using --clean.

       depend <file>
              Consider  the specified file as a dependency, so that its modification time will be
              checked.

       make <file> [<options>]
              Declare that the specified file has to be generated.  Options can specify  the  way
              it  should be produced, the available options are from <file> to specify the source
              and with <rule> to specify the conversion rule.  For instance, saying "make foo.pdf
              from foo.eps" indicates that foo.pdf should be produced from foo.eps, with any con‐
              version rule that can do it.  See the info documentation for details on  file  con‐
              version.

       module <module> [<options>]
              Loads  the specified module, possibly with options.  This is equivalent to the com‐
              mand-line option --module.

       onchange <file> <command>
              Execute the specified shell command after compiling if the contents of  the  speci‐
              fied file have changed.  The file name ends at the first space.

       paper <options>
              Specify  options related to paper size.  Currently they are used to give -t options
              to dvips and -p options to dvipdfm.

       path <directory>
              Adds the specified directory to the search path for TeX (and Rubber).  The name  of
              the directory is everything that follows the spaces after "path".

       produce <file>
              Declares that the LaTeX run will create or update the specified file(s).

       read <file>
              Read  the  specified  file  of directives.  The file must contain one directive per
              line.  Empty lines and lines that begin with "%" are ignored.

       rules <file>
              Read extra conversion rules from the specified file.  The format of  this  file  is
              the same as that of rules.ini, see the info documentation for details.

       set <name> <value>
              Set the value of a variable as a string.  For details on the existing variables and
              their meaning, see the info documentation.

       setlist <name> <values>
              Set the value of a variable as a (space-separated) list of strings.  For details on
              the existing variables and their meaning, see the info documentation.

       shell_escape
              Mark the document as requiring external programs (shell-escape or write18).  Rubber
              does not actually enable this unless called with the option --unsafe.

       synctex
              Enable SyncTeX support in the LaTeX run.

       watch <file>
              Watch the specified file for changes.  If the contents of  this  file  has  changed
              after  a compilation, then another compilation is triggered.  This is useful in the
              case of tables of contents, for instance.

   Module-specific directives
       If a command has the form foo.bar, it is considered a command bar for the module foo.   If
       this  module  is  not registered when the directive is found, then the command is silently
       ignored.  For the standard modules, the directives are the following:

       biblatex.path <directory>
              Adds the specified directory to the search path for BibTeX databases (.bib files).

       bibtex.crossrefs <number>
              Set the minimum number of crossref required for automatic inclusion of  the  refer‐
              enced entry in the citation list.  This sets the option -min-crossrefs when calling
              bibtex(1).

       bibtex.path <directory>
              Adds the specified directory to the search path for BibTeX databases (.bib files).

       bibtex.stylepath <directory>
              Adds the specified directory to the search path for BibTeX styles (.bst files).

       bibtex.tool <command>
              Use a different bibliography tool instead of BibTeX.

       dvipdfm.options <options>
              Pass the specified command-line switches to dvipdfm.

       dvips.options <options>
              Pass the specified command-line switches to dvips.

       index.tool (index) <name>
              Specifies which tool is to be used to process the index.  The  currently  supported
              tools  are  makeindex(1)  (the default choice) and xindy(1).  The argument index is
              optional, it may be used to specify the list of indexes  the  command  applies  to.
              When  present,  it  must  be  enclosed in parentheses; the list is comma-separated.
              When the argument is not present, the command applies to all indices.

       index.language (index) <language>
              Selects the language used for sorting the index.   This  only  applies  when  using
              xindy(1)  as  the  indexing  tool.  The optional argument has the same semantics as
              above.

       index.modules (index) <module>...
              Specify which modules to use when processing an index with xindy(1).  The  optional
              argument has the same semantics as above.

       index.order (index) <options>
              Modifies  the  sorting options for the indexes.  The arguments are words (separated
              by spaces) among standard, german and letter.  This only applies when using makein‐
              dex(1).  The optional argument has the same semantics as above.

       index.path (index) <directory>
              Adds the specified directory to the search path for index styles (.ist files).  The
              optional argument has the same semantics as above.

       index.style (index) <style>
              Specifies the index style to be used.  The optional argument has the same semantics
              as above.

       makeidx.language, .modules, .order, .path, .style, .tool
              These  directives  are  the  same  as  for the index module, except that they don't
              accept the optional argument.

       multibib.crossrefs, .path, .stylepath, .tool
              These directives are the same as for the bibtex module but they apply to bibliogra‐
              phies  managed by the multibib package.  They take an optional first argument, with
              the same convention as for the directives of the index module, which may be used to
              specify the list of bibliographies the command applies to.

BUGS
       There are surely a some...

       This  page  documents Rubber version 1.4.  The program and this man-page are maintained by
       Sebastian Kapfer <sebastian.kapfer@fau.de>.  The homepage  for  Rubber  can  be  found  at
       https://launchpad.net/rubber/.

SEE ALSO
       The full documentation for rubber is maintained as a Texinfo manual.  If the info and rub‐
       ber programs are properly installed at your site, the command

              info rubber

       should give you access to the complete manual.

                                                                                        RUBBER(1)
