abc2midi version 3.90 September 25 2016 abc2midi
Usage : abc2midi <abc file> [reference number] [-c] [-v] [-o filename]
        [-t] [-n <value>] [-CS] [-NFNP] [-NCOM] [-NFER] [-NGRA] [-HARP]
        [reference number] selects a tune
        -c  selects checking only
        -v  selects verbose option
        -ver prints version number and exits
        -o <filename>  selects output filename
        -t selects filenames derived from tune titles
        -n <limit> set limit for length of filename stem
        -CS use 2:1 instead of 3:1 for broken rhythms
        -quiet suppress some common warnings
        -silent suppresses most messages
        -Q default tempo (quarter notes/minute)
        -NFNP don't process !p! or !f!-like fields
        -NCOM suppress comments in output MIDI file
        -NFER ignore all fermata markings
        -NGRA ignore grace notes
        -STFW separate tracks for words (lyrics)
        -HARP ornaments=roll for harpist (same pitch)
        -BF Barfly mode: invokes a stress model if possible
        -OCC old chord convention (eg. +CE+)
        -TT tune to A =  <frequency>
        -CSM <filename> load custom stress models from file
 The default action is to write a MIDI file for each abc tune
 with the filename <stem>N.mid, where <stem> is the filestem
 of the abc file and N is the tune reference number. If the -o
 option is used, only one file is written. This is the tune
 specified by the reference number or, if no reference number
 is given, the first tune in the file.
