-= MikMod 3.2.8 =-
(c) 2004 Raphael Assenat and others - see file AUTHORS for complete list

Usage: mikmod [option|-y dir]... [module|playlist]...

Output options:
  -d[river] n,options     Use nth driver for output (0: autodetect), default: 0
  -o[utput] 8m|8s|16m|16s 8/16 bit output in stereo/mono, default: 16s
  -f[requency] nnnnn      Set mixing frequency, default: 44100
* -i[nterpolate]          Use interpolate mixing, default: Yes
* -hq[mixer]              Use high-quality (but slower) software mixer,
                          default: No
* -su[rround]             Use surround mixing, default: No
  -r[everb] nn            Set reverb amount (0-15), default: 0
Playback options:
  -v[olume] nn            Set volume from 0% (silence) to 100%, default: 100%
* -F, -fa[deout]          Force volume fade at the end of module, default: No
* -l[oops]                Enable in-module loops, default: No
* -a, -pa[nning]          Process panning effects, default: Yes
* -x, -pr[otracker]       Disable extended protracker effects, default: No
Loading options:
  -y, -di[rectory] dir    Scan directory recursively for modules
* -c[urious]              Look for hidden patterns in module, default: No
  -p[laymode] n           Playlist mode (1: loop module, 2: list multi
                             4: shuffle list, 8: list random), default: 2
* -t[olerant]             Don't halt on file access errors, default: Yes
Scheduling options (need root privileges or a setuid root binary):
* -s, -ren[ice]           Renice to -20 (more scheduling priority), default: No
* -S, -rea[ltime]         Get realtime priority (will hog CPU power), default: No
Display options:
  -q[uiet]                Quiet mode, no interface, displays only errors.
Information options:
  -n, -in[formation]      List all available drivers and module loaders.
  -N n, -drvinfo          Print information on a specific driver.
  -V -ve[rsion]           Display MikMod version.
  -h[elp]                 Display this help screen.
Configuration option:
  -norc                   Don't parse the file '~/.mikmodrc' on startup

Options marked with '*' also exist in negative form (eg -nointerpolate)
F1 or H while playing: Display help panel.
