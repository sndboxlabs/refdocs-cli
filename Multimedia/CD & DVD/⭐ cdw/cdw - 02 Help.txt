cdw 0.8.1
Copyright (C) 2002 - 2003 Balazs Varkonyi
Copyright (C) 2007 - 2016 Kamil Ignacak

License: GNU General Public License, version 2+

Usage: cdw [options]

Options:

  -h | --help               : show this message
  -v | --version            : show version of this software
  --escdelay=X              : modify ESC key delay period;
                              'X' is non-negative time in milliseconds
  --enable-dvd-rp-dl        : enable support for DVD+R DL;
                              (buggy, with dvd+rw-tools only)

