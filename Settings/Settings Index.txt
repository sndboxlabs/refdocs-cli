##################
# Settings Index #
##################

aconnect - ALSA sequencer connection manager
alsamixer - soundcard mixer for ALSA soundcard driver, with ncurses interface
btscanner - ncurses-based scanner for Bluetooth devices
dpkg-reconfigure - reconfigure an already installed package
hciconfig - configure Bluetooth devices
hcitool - configure Bluetooth connections
pacmd - reconfigure a PulseAudio sound server during runtime
pactl - control a running PulseAudio sound server
pamix - a pavucontrol inspired ncurses based pulseaudio mixer for the commandline
powertop - a power consumption and power management diagnosis tool
pulsemixer - a python-based pulseaudio mixer
setterm - set terminal attributes
wal - generate colorschemes on the fly
