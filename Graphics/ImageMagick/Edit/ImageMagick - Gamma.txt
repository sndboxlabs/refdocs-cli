#######################
# ImageMagick - Gamma #
#######################

    mogrify -gamma # "/path/to/image"

        * # = amount; use values between 0 to 3.0
        
Most systems use gamma of 2.2 by default; however, if your system uses
something like 1.8 (older Mac), then you may need to adjust the gamma to 
correctly view colors if you are going to share the image. Though be aware,
most modern systems automatically adjust this for you in image viewers.
