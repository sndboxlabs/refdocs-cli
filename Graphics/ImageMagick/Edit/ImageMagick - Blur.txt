######################
# ImageMagick - Blur #
######################

mogrify -blur <radius>x<strength> /path/to/image

    * radius = a square radius; blur this many neighboring pixels; maximum is 5
    * strength = blur by this amount; maximum is 8

See: https://www.imagemagick.org/Usage/blur/blur_montage.jpg for a better
understanding of how this works. If you are in TTY/console, you should
be able to use 'fbi' to display the image.
