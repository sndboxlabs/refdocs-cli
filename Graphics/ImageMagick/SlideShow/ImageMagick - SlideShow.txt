###########################
# ImageMagick - SlideShow #
###########################

animate -delay 500 -loop 0 -geometry 640x480\! '/path/to/folder/*'

    * -delay 500 = 5 seconds per image
    * -loop 0 = loop through the folder infinite amount of times
    * -geometry ....."\!" = force image sizes and stretch/skew if necessary
