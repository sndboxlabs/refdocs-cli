###############################
# ImageMaick - Convert Simple #
###############################

| Simple examples
`----------------

Convert PNG to JPEG:

    convert /path/to/image.png /path/to/image.jpg
    
Convert TIFF to BMP

    convert /path/to/image.tiff /path/to/image.bmp
    
Convert image folder to animated GIF:

    convert -delay 20 -loop 0 "/path/to/folder/*" -resize 640x480\! -background black -alpha remove "/path/to/image.gif"
    
        * -delay = time in x/100 seconds between each frame; 100 = 1 second
        * -loop 0 = infinite loop
        * -resize ...."\!" = forces the resolution and will stretch/skew images if needed
             **Just be careful because you may see tear lines in the middle if the image
               size ratios are too different
        * -background black = if there's an alpha channel, make it it black
        * -alpha remove = remove alpha channel

Convert PNG image to .ico file:

    convert -background transparent /path/to/image.png -define icon:auto-resize=16,32,48,64,128 /path/to/image.ico
