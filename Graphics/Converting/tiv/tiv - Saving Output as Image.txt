################################
# tiv - Saving Output as Image #
################################

Unfortunately, you cannot just use 'tiv image.png > out.txt' and have the
result append to a text file. I have no idea why not, but it can't. However,
you can use this trick:

    script "output.txt"
    tiv "/path/to/image"
    exit
    cat "output.txt"
    
The 'cat' part should print everything up until the exit part, which stops
the 'script' command. You can then open the 'output.txt' file and get rid
of everything except the part that actually shows the image. There is a 
tool for this called 'img2txt' that is part of 'caca-utils,' but truthfully,
it looks like crap. Caca-utils makes very heavy use of Latin characters
instead of just using color blocks like 'tiv' or 'catimg.'
