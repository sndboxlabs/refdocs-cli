##################
# Graphics Index #
##################

aewan - an ascii-art editor
album - make a web photo album
ansi2html - convert text with ANSI color codes to HTML
ansilove -  convert ANSi and artscene related file formats into PNG images
base64 - base64 encode/decode data and print to standard output
blender - a 3D modelling and rendering package
cacaview - ASCII image browser
cadubi - Creative ASCII Drawing Utility By Ian
cat - concatenate files and print on the standard output
catimg - fast image printing in to your terminal
chafa - character art facsimile generator
climage - convert images to beautiful ANSI escape codes
cowsay/cowthink - configurable speaking/thinking cow (and a bit more)
exiftool - read and write meta information in files
fbi - Linux framebuffer imageviewer
figlet - display large characters made up of ordinary screen characters
findimagedupes - finds visually similar or duplicate images
flydraw - an inline drawing tool
gifsicle - manipulates GIF images and animations
gnuplot - an interactive plotting program
green - an SDL PDF viewer that works in the framebuffer (console)
flydraw - an inline drawing tool
ImageMagick - image manipulation programs -- binaries
imagetracerjava - simple raster image tracer and vectorizer written in 
                  Java for desktop
img2pdf - lossless conversion of raster images to pdf
img2txt - convert images to various text-based coloured files
inkscape - an SVG (Scalable Vector Graphics) editing program
less - opposite of more
mapscii - a Braille & ASCII world map renderer for your console
pdftk - a handy tool for manipulating PDFs
pxltrm - a terminal pixel art editor written in pure bash
qrencode - encode input data in a QR Code and save as a PNG or EPS image
termgraph - a python command-line tool which draws basic graphs in the terminal
tiv - TerminalImageViewer: Small C++ program to display 
      images in a (modern) terminal using RGB ANSI codes 
      and unicode block graphic characters.
toilet - display large colourful characters
