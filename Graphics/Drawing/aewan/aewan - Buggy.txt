#################
# aewan - Buggy #
#################

aewan is kind of buggy. You'll have to save the "art" twice, using a
different name each time and the first saved file will then have some
bytes written to it. Alos, it is not cat-able like the man page claims.
You will have to use it as both editor and viewer.
