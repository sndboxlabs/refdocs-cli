############################
# telnet - Troubleshooting #
############################

Test connection to a website:
----------------------------

    telnet duckduckgo.com 80

    *The '80' part = port 80
    
    If it connects:
    
        Trying 184.72.104.138...
        Connected to duckduckgo.com.
        Escape character is '^]'

    If it fails (or something like this):

        Connecting To duckduckgo.com...Could not open connection to the host, 
        on port 80: Connect failed
