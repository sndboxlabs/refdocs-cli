mpsyt - Playlist

There are two good ways of doing this, either search for a playlist using 
'//PlaylistToSearchFor' and getting the playlist ID using 'i #' from the 
results, or you can play from a local file in which each line has a 
different URL that represents an item to play. You technically don't need 
mpsyt for that; you can also just use:

    mpv --no-video --ytdl-format="best[ext=mp4][height<=?720]" --playlist "/path/to/playlist.txt"

I guess what I'm getting at is that even though mpsyt works just fine on 
its own, you can also use it to grab specific URL's and create your own 
playlist and then just have mpv do the work. And because it's mpv, unlike 
vlc-nox, it has keyboard shortcuts that will work even in TTY/console. 
But if you do want a pretty ncurses interface for your playlist items you 
can just use:

    mpsyt url_file "/path/to/playlist.txt"
    
The beauty or advantage of just sticking with mpsyt, I guess, would be 
because it's readily available with a pip install if you can't rely on 
your package manager to have 'mps-youtube.' Also, you'll get fewer errors 
if mpsyt is what you used to get the URL's to add to the playlist. Just
be aware that it may take a minute for your playlist to show-up in the
mpsyt browser.
