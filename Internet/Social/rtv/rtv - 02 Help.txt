]2;rtv 1.22.1usage: rtv [URL] [-s SUBREDDIT]

  $ rtv https://www.reddit.com/r/programming/comments/7h9l31
  $ rtv -s linux

RTV (Reddit Terminal Viewer) is a terminal interface to view and interact with reddit.

positional arguments:
  URL               [optional] Full URL of a submission to open

optional arguments:
  -h, --help        show this help message and exit
  -s SUBREDDIT      Name of the subreddit that will be loaded on start
  --log FILE        Log HTTP requests to the given file
  --config FILE     Load configuration settings from the given file
  --ascii           Enable ascii-only mode
  --monochrome      Disable color
  --theme FILE      Color theme to use, see --list-themes for valid options
  --list-themes     List all of the available color themes
  --non-persistent  Forget the authenticated user when the program exits
  --clear-auth      Remove any saved user data before launching
  --copy-config     Copy the default configuration to
                    {HOME}/.config/rtv/rtv.cfg
  --copy-mailcap    Copy an example mailcap configuration to {HOME}/.mailcap
  --enable-media    Open external links using programs defined in the mailcap
                    config
  -V, --version     show program's version number and exit
  --no-flash        Disable screen flashing

Move the cursor using the arrow keys or vim style movement.
Press `?` to open the help screen.
