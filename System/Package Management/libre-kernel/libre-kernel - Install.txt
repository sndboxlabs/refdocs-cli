##########################
# libre-kernel - Install #
##########################

Debain:

    1. sudo apt edit-sources
    
    2. Add:
        deb mirror://linux-libre.fsfla.org/pub/linux-libre/freesh/mirrors.txt freesh main
        
    3. wget https://jxself.org/gpg.inc
    
    4. Check:
        gpg --with-fingerprint gpg.inc
        Key fingerprint: F611 A908 FFA1 65C6 9958 4ED4 9D0D B31B 545A 3198
        
    5. sudo apt-key add gpg.inc
       rm gpg.inc
       sudo apt update
       sudo apt install linux-libre-lts
