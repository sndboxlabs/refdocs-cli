cryptsetup 1.7.3
Usage: cryptsetup [OPTION...] <action> <action-specific>
      --version                         Print package version
  -v, --verbose                         Shows more detailed error messages
      --debug                           Show debug messages
  -c, --cipher=STRING                   The cipher used to encrypt the disk
                                        (see /proc/crypto)
  -h, --hash=STRING                     The hash used to create the encryption
                                        key from the passphrase
  -y, --verify-passphrase               Verifies the passphrase by asking for
                                        it twice
  -d, --key-file=STRING                 Read the key from a file.
      --master-key-file=STRING          Read the volume (master) key from file.
      --dump-master-key                 Dump volume (master) key instead of
                                        keyslots info.
  -s, --key-size=BITS                   The size of the encryption key
  -l, --keyfile-size=bytes              Limits the read from keyfile
      --keyfile-offset=bytes            Number of bytes to skip in keyfile
      --new-keyfile-size=bytes          Limits the read from newly added
                                        keyfile
      --new-keyfile-offset=bytes        Number of bytes to skip in newly added
                                        keyfile
  -S, --key-slot=INT                    Slot number for new key (default is
                                        first free)
  -b, --size=SECTORS                    The size of the device
  -o, --offset=SECTORS                  The start offset in the backend device
  -p, --skip=SECTORS                    How many sectors of the encrypted data
                                        to skip at the beginning
  -r, --readonly                        Create a readonly mapping
  -i, --iter-time=msecs                 PBKDF2 iteration time for LUKS (in ms)
  -q, --batch-mode                      Do not ask for confirmation
  -t, --timeout=secs                    Timeout for interactive passphrase
                                        prompt (in seconds)
  -T, --tries=INT                       How often the input of the passphrase
                                        can be retried
      --align-payload=SECTORS           Align payload at <n> sector boundaries
                                        - for luksFormat
      --header-backup-file=STRING       File with LUKS header and keyslots
                                        backup.
      --use-random                      Use /dev/random for generating volume
                                        key.
      --use-urandom                     Use /dev/urandom for generating volume
                                        key.
      --shared                          Share device with another
                                        non-overlapping crypt segment.
      --uuid=STRING                     UUID for device to use.
      --allow-discards                  Allow discards (aka TRIM) requests for
                                        device.
      --header=STRING                   Device or file with separated LUKS
                                        header.
      --test-passphrase                 Do not activate device, just check
                                        passphrase.
      --tcrypt-hidden                   Use hidden header (hidden TCRYPT
                                        device).
      --tcrypt-system                   Device is system TCRYPT drive (with
                                        bootloader).
      --tcrypt-backup                   Use backup (secondary) TCRYPT header.
      --veracrypt                       Scan also for VeraCrypt compatible
                                        device.
  -M, --type=STRING                     Type of device metadata: luks, plain,
                                        loopaes, tcrypt.
      --force-password                  Disable password quality check (if
                                        enabled).
      --perf-same_cpu_crypt             Use dm-crypt same_cpu_crypt
                                        performance compatibility option.
      --perf-submit_from_crypt_cpus     Use dm-crypt submit_from_crypt_cpus
                                        performance compatibility option.

Help options:
  -?, --help                            Show this help message
      --usage                           Display brief usage

<action> is one of:
	open <device> [--type <type>] [<name>] - open device as mapping <name>
	close <name> - close device (remove mapping)
	resize <name> - resize active device
	status <name> - show device status
	benchmark [--cipher <cipher>] - benchmark cipher
	repair <device> - try to repair on-disk metadata
	erase <device> - erase all keyslots (remove encryption key)
	luksFormat <device> [<new key file>] - formats a LUKS device
	luksAddKey <device> [<new key file>] - add key to LUKS device
	luksRemoveKey <device> [<key file>] - removes supplied key or key file from LUKS device
	luksChangeKey <device> [<key file>] - changes supplied key or key file of LUKS device
	luksKillSlot <device> <key slot> - wipes key with number <key slot> from LUKS device
	luksUUID <device> - print UUID of LUKS device
	isLuks <device> - tests <device> for LUKS partition header
	luksDump <device> - dump LUKS partition information
	tcryptDump <device> - dump TCRYPT device information
	luksSuspend <device> - Suspend LUKS device and wipe key (all IOs are frozen).
	luksResume <device> - Resume suspended LUKS device.
	luksHeaderBackup <device> - Backup LUKS device header and keyslots
	luksHeaderRestore <device> - Restore LUKS device header and keyslots

You can also use old <action> syntax aliases:
	open: create (plainOpen), luksOpen, loopaesOpen, tcryptOpen
	close: remove (plainClose), luksClose, loopaesClose, tcryptClose

<name> is the device to create under /dev/mapper
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action

Default compiled-in key and passphrase parameters:
	Maximum keyfile size: 8192kB, Maximum interactive passphrase length 512 (characters)
Default PBKDF2 iteration time for LUKS: 2000 (ms)

Default compiled-in device cipher parameters:
	loop-AES: aes, Key 256 bits
	plain: aes-cbc-essiv:sha256, Key: 256 bits, Password hashing: ripemd160
	LUKS1: aes-xts-plain64, Key: 256 bits, LUKS header hashing: sha256, RNG: /dev/urandom
