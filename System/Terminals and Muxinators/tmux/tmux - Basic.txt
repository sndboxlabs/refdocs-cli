################
# tmux - Basic #
################
This is just to help you get started. Look at the cheat sheets for more
options.

Split pane horizontally:
-----------------------

    Ctrl+b and then '"'

Split a panel vertically:
------------------------

    Ctrl+b and then '%'
    
Resize active pane:
------------------

    Ctrl+b and then ':'
    
        :resize-pane -D (Resizes the current pane down)
        :resize-pane -U (Resizes the current pane upward)
        :resize-pane -L (Resizes the current pane left)
        :resize-pane -R (Resizes the current pane right)
        :resize-pane -D 10 (Resizes the current pane down by 10 cells)
        :resize-pane -U 10 (Resizes the current pane upward by 10 cells)
        :resize-pane -L 10 (Resizes the current pane left by 10 cells)
        :resize-pane -R 10 (Resizes the current pane right by 10 cells)
        
Switch between panes:
--------------------

    Ctrl+b and then 'o'
    
Enable mouse for switching between panes (GUI only):
---------------------------------------------------

    tmux set -g mouse on
    
    *This also allows you to click and drag the split to adjust pane sizes
