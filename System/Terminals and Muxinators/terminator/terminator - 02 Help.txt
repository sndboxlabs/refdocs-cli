Usage: terminator [options]

Options:
  -h, --help            show this help message and exit
  -v, --version         Display program version
  -m, --maximise        Maximize the window
  -f, --fullscreen      Make the window fill the screen
  -b, --borderless      Disable window borders
  -H, --hidden          Hide the window at startup
  -T FORCEDTITLE, --title=FORCEDTITLE
                        Specify a title for the window
  --geometry=GEOMETRY   Set the preferred size and position of the window(see
                        X man page)
  -e COMMAND, --command=COMMAND
                        Specify a command to execute inside the terminal
  -g CONFIG, --config=CONFIG
                        Specify a config file
  -x, --execute         Use the rest of the command line as a command to
                        execute inside the terminal, and its arguments
  --working-directory=DIR
                        Set the working directory
  -c CLASSNAME, --classname=CLASSNAME
                        Set a custom name (WM_CLASS) property on the window
  -i FORCEDICON, --icon=FORCEDICON
                        Set a custom icon for the window (by file or name)
  -r ROLE, --role=ROLE  Set a custom WM_WINDOW_ROLE property on the window
  -l LAYOUT, --layout=LAYOUT
                        Launch with the given layout
  -s, --select-layout   Select a layout from a list
  -p PROFILE, --profile=PROFILE
                        Use a different profile as the default
  -u, --no-dbus         Disable DBus
  -d, --debug           Enable debugging information (twice for debug server)
  --debug-classes=DEBUG_CLASSES
                        Comma separated list of classes to limit debugging to
  --debug-methods=DEBUG_METHODS
                        Comma separated list of methods to limit debugging to
  --new-tab             If Terminator is already running, just open a new tab
