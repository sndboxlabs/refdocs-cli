TTY-CLOCK(1)                              User Commands                              TTY-CLOCK(1)

NAME
       tty-clock - a terminal digital clock

SYNOPSIS
       tty-clock [-iuvsScbtrahDBxn] [-C [0-7]] [-f format] [-d delay] [-a nsdelay] [-T tty]

DESCRIPTION
       tty-clock displays a simple digital clock on the terminal. Invoked without options it will
       display the clock on the upper left corner of the screen on the terminal it  was  executed
       from.

COMMANDS
       tty-clock  accepts  a number of runtime keyboard commands, upper and lower case characters
       are treated identically.

       K,J,H,L
              vi-style movement commands to set the position of the displayed clock.  These  com‐
              mands have no effect when the centered option is set.

       [0-7]  Select a different color for displaying the clock.

       B      Toggles bewteen bold and normal colors.

       X      Toggles displaying a box around the clock. This option is disabled by default.

       C      Toggle  the  clock's position to centered.  When set the movement commands are dis‐
              abled.

       R      Set the clock to rebound along the edges of the terminal.

       S      Display seconds.

       T      Switch time output to the 12-hour format.

       Q      Quit.

OPTIONS
       -s     Show seconds.

       -S     Screensaver mode. tty-clock terminates when any key is pressed.

       -x     Show box.

       -c     Set the clock at the center of the terminal.

       -C [0-7]
              Set the clock color.

       -b     Use bold colors.

       -t     Set the hour in 12h format.

       -u     Use UTC time.

       -T tty Display the clock on the given tty. tty must be a valid character device  to  which
              the user has rw access permissions.  (See EXAMPLES)

       -r     Do rebound the clock.

       -f format
              Set the date format as described in strftime(3).

       -n     Do not quit the program when the Q key is pressed (or when any key is pressed while
              in Screensaver mode). A signal must be sent to tty-clock in order to terminate  its
              execution. (See EXAMPLES)

       -v     Show tty-clock version.

       -i     Show some info about tty-clock.

       -h     Show usage information.

       -D     Hide the date.

       -B     Enable blinking colon.

       -d delay
              Set the delay (in seconds) between two redraws of the clock. Default 1s.

       -a nsdelay
              Additional delay (in nanoseconds) between two redraws of the clock. Default 0ns.

EXAMPLES
       To  invoke  tty-clock  in  screensaver  mode with the clock display set to rebound and the
       update delay set to 1/10th of a second (10 FPS):

              $ tty-clock -Sra 100000000 -d 0

       The following example arranges for tty-clock to be displayed indefinitely on  one  of  the
       Virtual Terminals on a Linux system at boot time using an inittab(5) entry:

              # /etc/inittab:
              9:2345:respawn:/usr/bin/tty-clock -c -n -T /dev/tty9

                                           October 2013                              TTY-CLOCK(1)
