TermSaver v.0.3 - A simple text-based terminal screensaver.

Usage: termsaver [screen] [options]

Screens:

 quotes4all     displays recent quotes from quotes4all.net
 randtxt        displays word in random places on screen
 starwars       runs the asciimation Star Wars movie
 clock          displays a digital clock on screen
 urlfetcher     displays url contents with typing animation
 rssfeed        displays rss feed information
 programmer     displays source code in typing animation
 jokes4all      displays recent jokes from jokes4all.net (NSFW)
 rfc            randomly displays RFC contents
 asciiartfarts  displays ascii images from asciiartfarts.com (NSFW)
 matrix         displays a matrix movie alike screensaver
 sysmon         displays a graphical system monitor

Options:

 -h, --help     Displays this help message
 -v, --verbose  Displays python exception errors (for debugging)

Refer also to each screen's help by typing: termsaver [screen] -h

--
See more information about this project at:
http://termsaver.info

Report bugs to authors at:
http://github.com/brunobraga/termsaver

