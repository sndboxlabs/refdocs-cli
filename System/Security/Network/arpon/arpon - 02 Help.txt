
Usage: arpon [Options] {SARPI | DARPI | HARPI}

TASK MODE
  -n, --nice             <Nice>         Sets PID's CPU priority
                                        (Default Nice: 0)
  -p, --pid-file         <Pid file>     Sets the pid file
                                        (Default: /var/run/arpon.pid)
  -q, --quiet                           Works in background task

LOG MODE
  -f, --log-file         <Log file>     Sets the log file
                                        (Default: /var/log/arpon.log)
  -g, --log                             Works in logging mode

DEVICE MANAGER
  -i, --iface            <Iface>        Sets your device manually
  -o, --iface-auto                      Sets device automatically
  -l, --iface-list                      Prints all supported interfaces

STATIC ARP INSPECTION
  -c, --sarpi-cache      <Cache file>   Sets SARPI entries from file
                                        (Default: /etc/arpon.sarpi)
  -x, --sarpi-timeout    <Timeout>      Sets SARPI Cache refresh timeout
                                        (Default: 5 minuts)
  -S, --sarpi                           Manages ARP Cache statically

DYNAMIC ARP INSPECTION
  -y, --darpi-timeout    <Timeout>      Sets DARPI entries response max timeout
                                        (Default: 5 seconds)
  -D, --darpi                           Manages ARP Cache dynamically

HYBRID ARP INSPECTION
  -c, --sarpi-cache      <Cache file>   Sets HARPI entries from file
                                        (Default: /etc/arpon.sarpi)
  -x, --sarpi-timeout    <Timeout>      Sets HARPI Cache refresh timeout
                                        (Default: 5 minuts)
  -y, --darpi-timeout    <Timeout>      Sets HARPI entries response max timeout
                                        (Default: 5 seconds)
  -H, --harpi                           Manages ARP Cache
                                        statically and dynamically

MISC FEATURES
  -e, --license                         Prints license page
  -v, --version                         Prints version number
  -h, --help                            Prints help summary page

SEE THE MAN PAGE FOR MANY DESCRIPTIONS AND EXAMPLES

