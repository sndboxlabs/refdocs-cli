Usage: debsecan OPTIONS...

Options:
  -h, --help            show this help message and exit
  --config=FILE         sets the name of the configuration file
  --suite=SUITE         set the Debian suite of this installation
  --source=URL          sets the URL for the vulnerability information
  --status=NAME         name of the dpkg status file
  --format=FORMAT       change output format
  --only-fixed          list only vulnerabilities for which a fix is available
  --no-obsolete         do not list obsolete packages (not recommend)
  --history=NAME        sets the file name of debsecan's internal status file
  --line-length=LINE_LENGTH
                        maximum line length in report mode
  --update-history      update the history file after reporting
  --mailto=MAILTO       send report to an email address
  --cron                debsecan is invoked from cron
  --whitelist=NAME      sets the name of the whitelist file
  --add-whitelist       add entries to the whitelist
  --remove-whitelist    remove entries from the whitelist
  --show-whitelist      display entries on the whitelist
  --disable-https-check
                        disable certificate checks
  --update-config       
