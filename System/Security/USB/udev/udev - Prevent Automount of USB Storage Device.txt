##################################################
# udev - Prevent Automount of USB Storage Device #
##################################################

# more /etc/udev/rules.d/85-no-automount.rules
SUBSYSTEM=="usb", ENV{UDISKS_AUTO}="0"

Then reboot

HOWEVER:

    If you are signed into a desktop environment, you may also have to
    disable automount using your file manager's preferences. I can only
    confirm that the "udev" method works as long as you stay in TTY/console.
    I also chose the udev method because it seems to work on all kinds
    of Linux systems as opposed to using fstab, gconftool-2, fdi, polkit,
    etc.
