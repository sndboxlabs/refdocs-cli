################
# lsof - Basic #
################

List active tcp port 80:
-----------------------

    lsof -i tcp:80
    
List active tcp port 443:
------------------------

    lsof -i tcp:443
    
List all active port 80 connections:
-----------------------------------

    lsof -i :80

All the service/process using port 80:
-------------------------------------
lsof -i TCP:80 -s TCP:LISTEN

List programs using Internet:
----------------------------
lsof -P -i -n
